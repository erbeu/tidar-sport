<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<div class="row produk">
    <?php
    // jika pembeli mencari produk
    if(isset($_GET["cari"])) {
        // ambil produk dengan stok tersedia dan sesuai nama yg dicari
        $q = mysqli_query($conn, "SELECT * FROM produk WHERE stok > 0 AND nama_produk LIKE '%$_GET[cari]%'");
    } else {
        // ambil semua produk dengan stok tersedia
        $q = mysqli_query($conn, "SELECT * FROM produk WHERE stok > 0");
    }
    
    // looping produk
    while($d = mysqli_fetch_array($q)) {
        // tampilkan data produk
        echo "
            <div class='col-md-3 produk-box'>
            <img src='assets/img/produk/$d[gambar]' class='img-responsive gambar'>
                <h3>".substr($d["nama_produk"], 0, 17)."</h3>
                <p>".substr($d["deskripsi"], 0, 50)." ..</p>
                <p>".format_rupiah($d["harga"])."</p>
                <a class='btn btn-primary btn-block' href='index.php?halaman=produk-detail&id=$d[id]' role='button'>Detail &raquo;</a>
            </div>";
    }
    ?>
</div>