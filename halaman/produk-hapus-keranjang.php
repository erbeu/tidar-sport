<?php
// hapus produk di keranjang
unset($_SESSION["id_produk"]);
unset($_SESSION["jumlah"]);

// reload halaman dan tampilkan pesan berhasil
header("location:index.php?halaman=produk&msg=Produk berhasil dihapus dari keranjang");
