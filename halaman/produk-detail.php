<div class="row produk produk-detail">
    <?php
    // ambil data ukuran
    $ukuran = "";
    foreach(ukuran() as $data) {
        $ukuran .= "<option>$data</option>";
    }

    // ambil data produk yg dipilih
    $q = mysqli_query($conn, "SELECT * FROM produk WHERE id = $_GET[id]");
    $d = mysqli_fetch_array($q);

    // update stok dari keranjang
    if (isset($_SESSION["id_produk"])) {
        $stok_pesanan = 0;
        $a = array_keys($_SESSION["id_produk"], $_GET["id"]);
        foreach($a as $key => $value) {
            $stok_pesanan += $_SESSION["jumlah"][$value];
        }
        $d["stok"] -= $stok_pesanan;
    }

    // tampilkan data produk
    echo "
        <div class='col-md-5'>
            <img src='assets/img/produk/$d[gambar]' class='img-responsive'>
        </div>
        <div class='col-md-7'>
            <h3>$d[nama_produk]</h3>
            <p>$d[deskripsi]</p>
            <p>Stok : $d[stok]</p>
            <p class='harga'>".format_rupiah($d["harga"])."</p>
            <form action='index.php?halaman=produk-beli&id=$d[id]' method='post'>
                <input type='number' name='jumlah' class='form-control' placeholder='Jumlah' min='1' max='$d[stok]' required>
                <select name='ukuran' class='form-control' required>
                    <option value=''>-- Pilih Ukuran--</option>
                    $ukuran
                </select>
                <input type='submit' name='submit' value='Beli' class='btn btn-primary'>
            </form>
            <hr>
            <a class='btn btn-default btn-block' href='index.php?halaman=produk' role='button'>Kembali</a>
        </div>";
    ?>
</div>
