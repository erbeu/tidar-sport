<?php
// ambil data tarif
$q_tarif = mysqli_query($conn, "SELECT * FROM tarif WHERE id = '$_POST[id_tarif]'");
$d_tarif = mysqli_fetch_array($q_tarif);

// simpan pemesanan
$q = mysqli_query($conn, "INSERT INTO pemesanan VALUES(
    null,
    '$_SESSION[id_pembeli]',
    '1',
    '$d_tarif[id_kurir]',
    '$d_tarif[id_kota]',
    '$_POST[ongkir_hidden]',
    '$_POST[alamat_kirim]',
    '".date("Y-m-d G:i:s")."',
    null
)");
$id_pemesanan = mysqli_insert_id($conn);

// looping sesuai jumlah produk
for($i = 0; $i < count($_SESSION["id_produk"]); $i++) {
    // ambil jumlah stok terakhir dari tabel produk
    $q0 = mysqli_query($conn, "SELECT * FROM produk WHERE id = '".$_SESSION["id_produk"][$i]."'");
    $d0 = mysqli_fetch_array($q0);

    // simpan produk yang dipesan
    $q1 = mysqli_query($conn, "INSERT INTO pemesanan_detail VALUES(
        null,
        $id_pemesanan,
        ".$_SESSION["id_produk"][$i].",
        ".$d0["stok"].",
        ".$_SESSION["jumlah"][$i].",
        '".$_SESSION["ukuran"][$i]."'
    )");

    // kurangi stok produk yang dipesan
    $q2 = mysqli_query($conn, "UPDATE produk SET
        stok = (stok - ".$_SESSION["jumlah"][$i].")
        WHERE id = '".$_SESSION["id_produk"][$i]."'
    ");
}

// hapus keranjang
unset($_SESSION["id_produk"]);
unset($_SESSION["jumlah"]);
unset($_SESSION["ukuran"]);

// alihkan ke halaman checkout selesai
header("location:index.php?halaman=checkout-selesai&id=$id_pemesanan");
