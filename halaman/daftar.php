<?php
// cek jika tombol disubmit
if(isset($_POST["submit"])) {
    // simpan data pembeli
    $q = mysqli_query($conn, "INSERT INTO pembeli VALUES(
        null,
        '$_POST[nama]',
        '$_POST[alamat]',
        '$_POST[email]',
        '$_POST[telepon]',
        '$_POST[username]',
        MD5('$_POST[password]')
        )");
    
    // jika berhasil, alihkan ke halaman utama, jika tidak, tampilkan error
    if($q) {
        header("location:index.php?msg=Pendaftaran berhasil, silahkan login");
    } else {
        echo "<div class=\"alert alert-danger\">Registrasi Gagal</div>";
    }
}
?>

<h3>Pendaftaran Member</h3>
<form action="" method="post">
    <label for="">Nama Lengkap</label>
    <input type="text" name="nama" class="form-control" required>
    <br>
    <label for="">Alamat</label>
    <textarea name="alamat" id="" cols="30" rows="10" class="form-control" required></textarea>
    <br>
    <label for="">Email</label>
    <input type="email" name="email" class="form-control" required>
    <br>
    <label for="">No. Telepon</label>
    <input type="text" name="telepon" class="form-control" required>
    <br>
    <label for="">Username</label>
    <input type="text" name="username" class="form-control" required>
    <br>
    <label for="">Password</label>
    <input type="password" name="password" class="form-control" required>
    <br>
    <input type="submit" name="submit" value="Daftar" class="btn btn-primary">
</form>