<?php
// cek jika tombol kirim bukti di submit
if(isset($_POST["submit1"])) {
    // beri nama foto
    $foto = "c-$_GET[id].jpg";

    // tentukan lokasi upload
    $target_file = "assets/img/konfirmasi/$foto";

    // cek foto atau bukan
    $check = getimagesize($_FILES["foto"]["tmp_name"]);

    // cek jika foto berhasil di upload
    if($check && move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
        // simpan konfirmasi pembayaran custom
        $q = mysqli_query($conn, "INSERT INTO konfirmasi_pembayaran_custom VALUES(
            null,
            '$_GET[id]',
            '$foto',
            '".date("Y-m-d H:i:s")."'
            )");

        // reload halaman dan beri pesan bahwa pesanan akan diproses
        header("location:index.php?halaman=konfirmasi-pembayaran-custom&msg=Terimakasih, pesanan anda akan segera kami proses");
    } else {
        // jika gagal upload, tampilkan error
        $msg = "Gagal upload";
    }
}
?>

<h3>Konfirmasi Pembayaran Custom Order</h3>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<form action="" method="get">
    <input type="hidden" name="halaman" value="konfirmasi-pembayaran-custom">
    <label for="">No Pemesanan</label>
    <div class="input-group">
        <span class="input-group-addon" id="id">C-</span>
        <input type="number" name="id" class="form-control" aria-describedby="id" required>
    </div><br>
    <input type="submit" name="submit" value="Cek Nomor" class="btn btn-primary">
</form>
<hr>
<?php
// cek jika tombol cek nomor di submit
if(isset($_GET["submit"])) {
    // ambil file cetak nota custom
    require_once("custom-order-cetak-nota.php");

    // cek apakah proses pemesanan custom sudah selesai atau belum
    if($id_status != 1) {
        // jika belum selesai tampilkan error
        $q1 = mysqli_query($conn, "SELECT * FROM status WHERE id = $id_status");
        $d1 = mysqli_fetch_array($q1);
        echo "<div class='alert alert-danger'>Maaf, anda tidak dapat melakukan konfirmasi pembayaran karena status pemesanan <kbd>$d1[nama_status]</kbd>.</div>";
    } else {
        // cek jika nomor pemesanan ada
        if(mysqli_num_rows($q) != 0) {
            // tampilkan form upload bukti transfer
            ?>
            <hr>
            <form action="" method="post" enctype="multipart/form-data">
                <label for="">Bukti Transfer (Foto)</label>
                <input type="file" name="foto" class="form-control" required><br>
                <input type="submit" name="submit1" value="Kirim Bukti Transfer" class="btn btn-primary">
            </form>
            <?php
        }
    }
}
?>
