<div class="sidebar-box">
    <h4>Member</h4>
    <?php
    // tampilkan pesan error jika ada
    echo isset($_GET["errormsg"]) ? "<div class='alert alert-danger'>$_GET[errormsg]</div>" : "";

    // cek pembeli sudah login belum
    if(empty($_SESSION["id_pembeli"])) {
        // jika belum login, tampilkan form login
        ?>
        <form action="index.php?halaman=login" method="post">
            <label for="">Username</label>
            <input type="text" name="username" class="form-control">
            <br>
            <label for="">Password</label>
            <input type="password" name="password" class="form-control">
            <br>
            <input type="submit" name="submit" value="Login" class="btn btn-primary btn-block">
            <a href="index.php?halaman=daftar" class="btn btn-link btn-block">Daftar Member Disini (Gratis)</a>
        </form>
        <?php
    } else {
        // jika sudah, tampilkan pesan selamat datang
        echo "
            <p>Halo $_SESSION[nama_pembeli], selamat berbelanja!<p>
            <a href='index.php?halaman=logout' class='btn btn-danger'>Logout</a>
        ";
    }
    ?>
</div>
<div class="sidebar-box">
    <h4>Cari Produk</h4>
    <form action="" method="get">
        <input type="hidden" name="halaman" value="produk">
        <input type="text" name="cari" value="<?php echo isset($_GET["cari"]) ? $_GET["cari"] : ""; ?>" class="form-control" placeholder="Ketik disini & tekan enter">
    </form>
</div>
<div class="sidebar-box">
    <h4>Keranjang Belanja</h4>
    <?php
    $total_harga = 0;
    $diskon = 0;
    $jumlah = 0;

    // masukkan jumlah produk jika ada produk dikeranjang
    $keranjang = isset($_SESSION["id_produk"]) ? count($_SESSION["id_produk"]) : 0;

    // cek jika keranjang tidak kosong
    if (!empty($keranjang)) {
        // looping produk di keranjang
        for($i = 0; $i < $keranjang; $i++) {
            // ambil produk
            $q = mysqli_query($conn, "SELECT harga FROM produk WHERE id = ".$_SESSION["id_produk"][$i]);
            $d = mysqli_fetch_array($q);

            // // masukkan jumlah produk
            $jumlah += $_SESSION["jumlah"][$i];

            // hitung harga per produk dikali jumlahnya
            $harga[$i] = $d["harga"] * $_SESSION["jumlah"][$i];

            // total harga = total harga sebelumnya + harga per produk
            $total_harga += $harga[$i];
        }

        // cek jumlah pembelian apakah melebihi minimum diskon
        if (array_sum($_SESSION["jumlah"]) >= $config["min_diskon"]) {
            // jika jumlah melebihi minimum diskon maka harga akan dikurangi diskon
            $diskon = $total_harga * $config["persen_diskon"];
            $total_harga = $total_harga - $diskon;
        }
    }
    ?>
    <table class="table table-bordered">
        <tr>
            <td width="50%">Jumlah Produk</td>
            <td>: <?php echo $jumlah; ?> Barang</td>
        </tr>
        <tr>
            <td>Diskon</td>
            <td>: <?php echo format_rupiah($diskon); ?></td>
        </tr>
        <tr>
            <td>Total Harga</td>
            <td>: <?php echo format_rupiah($total_harga); ?></td>
        </tr>
    </table>
    <a href="index.php?halaman=checkout" class="btn btn-primary btn-block">Proses ke checkout</a>
    <a href="index.php?halaman=produk-hapus-keranjang" class="btn btn-danger btn-block">Hapus barang di keranjang</a>
</div>
<div class="sidebar-box">
    <h4>Pembayaran</h4>
    <img src="assets/img/bca.jpg" alt="">
</div>
<div class="sidebar-box">
    <h4>Pengiriman</h4>
    <img src="assets/img/jne.png" alt="">
</div>
