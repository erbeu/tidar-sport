<h3>Cara Pembelian</h3>
<ul>
    <li>Login terlebih dahulu, jika anda belum terdaftar sebagai member, maka silakan mendaftar dengan pilih menu <b>Daftar Member Disini (Gratis)</b>.</li>
    <li>Kemudian isi data yang tersedia dengan benar, dan pilih <b>Daftar</b>.</li>
    <li>Kemudian jika sudah terdaftar maka silakan login untuk memulai berbelanja.</li>
    <li>Kemudian pilih menu <b>Produk</b> untuk melihat produk yang tersedia, jika sudah pilih <b>Detail</b> kemudian tentukan jumlah dan ukuran yang inginkan dan pilih menu <b>Beli</b>.</li>
    <li>Kemudian pesanan anda akan muncul di keranjang belanja, kemudian pilih <b>Proses Ke Checkout</b>.</li>
	<li>Kemudian detail pesanan anda akan muncul, lalu pilih kota tujuan dan isi alamat lengkap untuk pengiriman barang pesanan jika sudah pilih <b>Pesan Barang</b>.</li>
	<li>Kemudian <b>Cetak Nota</b> sebagai simpanan anda.</li>
</ul>
<h3>Terimakasih, dan selamat berbelanja..</h3>