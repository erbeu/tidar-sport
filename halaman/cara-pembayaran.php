<h3>Cara Pembayaran</h3>
<ul>
    <li>Silakan melakukan pembayaran ke Rekening <b>BCA 4411263368 A/N Habib</b></li>
	<li>Jika dalam waktu 3 hari tidak melakukan pembayaran maka pembelian dianggap dibatalkan.</li>
	<li>Jika sudah transfer, kemudian konfirmasi dengan pilih menu <b>Konfirmasi Pembayaran</b>, dan isi nomor pesanan anda di nomer pemesanan lalu pilih <b>Cek Nomor</b>.</li>
	<li>Kemudian akan muncul detail pesanan anda, dan upload bukti transfer pembayaran di <b>Browse</b> dan pilih <b>Kirim Bukti Transfer</b>.</li>
	<li>Kemudian jika tidak ada masalah, maka barang akan segera dikirim.</li>
</ul>
<h3>Terimakasih, dan selamat berbelanja..</h3>