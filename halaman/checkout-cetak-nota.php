<?php
// ambil data pemesanan dari database sesuai no pemesanan
$q = mysqli_query($conn, "SELECT *, pemesanan.id as id_pemesanan FROM pemesanan
    JOIN status ON status.id = pemesanan.id_status
    JOIN pembeli ON pembeli.id = pemesanan.id_pembeli
    JOIN kurir ON kurir.id = pemesanan.id_kurir
    JOIN kota ON kota.id = pemesanan.id_kota
    where pemesanan.id = '$id'") or die(mysqli_error($conn));

// cek ada pemesanan dengan no tersebut atau tidak
if(mysqli_num_rows($q) == 0) {
    // jika tidak ada tampilkan error
    echo "<div class='alert alert-danger'>Tidak ada pesanan dengan nomor tersebut.</div>";
} else {
    // jika ada ambil data pemesanan dan tampilkan
    $d = mysqli_fetch_array($q);
    $biaya_kirim = $d["biaya_kirim"];
    $id_status = $d["id_status"];
    $waktu_sampai = $d["waktu_sampai"];
    ?>

    <h3>Detail Pemesanan TIDAR SPORT</h3>
    <hr>
    <table class="table table-bordered produk">
        <tr>
            <td width="30%">No Pemesanan</td>
            <td>P-<?php echo $d["id_pemesanan"]; ?></td>
        </tr>
        <tr>
            <td width="30%">Nama Pembeli</td>
            <td><?php echo $d["nama_pembeli"]; ?></td>
        </tr>
        <tr>
            <td>Kurir</td>
            <td><?php echo $d["nama_kurir"]; ?></td>
        </tr>
        <tr>
            <td>Kota</td>
            <td><?php echo $d["nama_kota"]; ?></td>
        </tr>
        <tr>
            <td>Biaya Kirim</td>
            <td><?php echo format_rupiah($d["biaya_kirim"]); ?></td>
        </tr>
        <tr>
            <td>Alamat Kirim</td>
            <td><?php echo $d["alamat_kirim"]; ?></td>
        </tr>
        <tr>
            <td>Waktu</td>
            <td><?php echo $d["waktu"]; ?></td>
        </tr>
        <tr>
            <td>Status</td>
            <td><?php echo $d["nama_status"]; ?></td>
        </tr>
    </table>
    <hr>
    <table class="table table-bordered produk">
        <tr>
            <th>No</th>
            <th>Gambar</th>
            <th>Produk</th>
            <th>Ukuran</th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th>Sub Total</th>
        </tr>
        <?php
        $no = 1;
        $jumlah_produk = 0;
        $total_harga = 0;
        $jumlah_harga = 0;
        $diskon = 0;
        $jumlah_dibayar = 0;

        // ambil produk-produk yg dipesan
        $q = mysqli_query($conn, "SELECT * FROM pemesanan_detail
            JOIN produk ON produk.id = pemesanan_detail.id_produk
            WHERE id_pemesanan = $id");

        // looping data produk yg dipesan
        while($d = mysqli_fetch_array($q)) {
            // jumlah produk = jumlah produk sebelumnya + jumlah produk yg dipesan
            $jumlah_produk += $d["jumlah"];

            // total harga = harga produk * jumlah produk yg dipesan
            $total_harga = $d["harga"] * $d["jumlah"];

            // jumlah harga = jumlah harga sebelumnya + total harga
            $jumlah_harga += $total_harga;

            // tampilkan data produk
            echo "
                <tr>
                    <td>$no</td>
                    <td><img src='assets/img/produk/$d[gambar]' width='100px'></td>
                    <td>$d[nama_produk]</td>
                    <td>$d[ukuran]</td>
                    <td>".format_rupiah($d["harga"])."</td>
                    <td>$d[jumlah]</td>
                    <td>".format_rupiah($total_harga)."</td>
                </tr>
            ";
            $no++;
        }

        $jumlah_dibayar = $jumlah_harga;

        // cek jumlah pembelian apakah melebihi minimum diskon
        if ($jumlah_produk >= $config["min_diskon"]) {
            // diskon = jumlah harga * persen diskon
            $diskon = $jumlah_harga * $config["persen_diskon"];
            // jika jumlah melebihi minimum diskon maka harga akan dikurangi diskon
            $jumlah_dibayar = $jumlah_harga - $diskon;
        }

        // tampilkan jumlah produk, diskon, biaya kirim, dan jumlah yang dibayar
        echo "
            <tr>
                <td colspan='5'>Jumlah</td>
                <td>".$jumlah_produk." (".ceil($jumlah_produk/4)." kg)</td>
                <td>".format_rupiah($jumlah_harga)."</td>
            </tr>
            <tr>
                <td colspan='6'>Diskon / Potongan Harga</td>
                <td>".format_rupiah($diskon)."</td>
            </tr>
            <tr>
                <td colspan='6'>Biaya Kirim</td>
                <td>".format_rupiah($biaya_kirim)."</td>
            </tr>
            <tr class='total'>
                <td colspan='6'>Jumlah yang harus dibayar</td>
                <td>".format_rupiah($biaya_kirim + $jumlah_dibayar)."</td>
            </tr>
        ";
        ?>
    </table>
<?php } ?>
