<?php
// jika pembeli belum login, maka alihkan ke halaman utama dengan pesan
if(empty($_SESSION["id_pembeli"])) header("location:index.php?msg=Anda harus login dahulu");
?>

<h3>Checkout</h3>
<table class="table table-bordered produk">
    <tr>
        <th>No</th>
        <th>Gambar</th>
        <th>Produk</th>
        <th>Ukuran</th>
        <th>Harga</th>
        <th>Jumlah</th>
        <th>Total Harga</th>
    </tr>
    <?php
    // inisiasi
    $total_harga = 0;
    $jumlah_harga = 0;
    // looping setiap produk yg dipesan
    for($i = 0; $i < count($_SESSION["id_produk"]); $i++) {
        // ambil data produk dari database
        $q = mysqli_query($conn, "SELECT * FROM produk WHERE id = ".$_SESSION["id_produk"][$i]);
        $d = mysqli_fetch_array($q);

        // hitung harga per produk dikali jumlahnya
        $total_harga = $d["harga"] * $_SESSION["jumlah"][$i];

        // jumlah harga diisi dengan jumlah harga sebelumnya + harga per produk
        $jumlah_harga += $total_harga;

        // tampilkan detail produk
        echo "
            <tr>
                <td>".($i+1)."</td>
                <td><img src='assets/img/produk/$d[gambar]' width='100px'></td>
                <td>$d[nama_produk]</td>
                <td>".$_SESSION["ukuran"][$i]."</td>
                <td>".format_rupiah($d["harga"])."</td>
                <td>".$_SESSION["jumlah"][$i]."</td>
                <td>".format_rupiah($total_harga)."</td>
            </tr>
        ";
    }

    $jumlah_dibayar = $jumlah_harga;

    // cek jumlah pembelian apakah melebihi minimum diskon
    if (array_sum($_SESSION["jumlah"]) >= $config["min_diskon"]) {
        // jika jumlah melebihi minimum diskon maka harga akan dikurangi diskon
        $diskon = $jumlah_harga * $config["persen_diskon"];
        $jumlah_dibayar = $jumlah_harga - $diskon;
    }

    // tampilkan jumlah harga, diskon, dan jumlah yang harus dibayar
    echo "
        <tr>
            <td colspan='5'>Jumlah</td>
            <td>".array_sum($_SESSION["jumlah"])." (".ceil(array_sum($_SESSION["jumlah"])/4)." kg)</td>
            <td>".format_rupiah($jumlah_harga)."</td>
        </tr>
        <tr>
            <td colspan='6'>Diskon / Potongan Harga</td>
            <td>".format_rupiah($diskon)."</td>
        </tr>
        <tr>
            <td colspan='6'>Jumlah yang harus dibayar (belum termasuk ongkir)</td>
            <td>".format_rupiah($jumlah_dibayar)."</td>
        </tr>
    ";

    // ajax untuk mengambil ongkir dan menghitung total harga
    $js = "
        $('#ongkir').number(true);
        $('#total').number(true);
        $('#id_tarif').on('change', function() {
            var total_barang = ".array_sum($_SESSION["jumlah"]).";
            var total_harga = $jumlah_dibayar;
            var id_tarif = $(this).val();
            $.ajax({
                url: 'ajax.php?get=ongkir&total_barang='+total_barang+'&total_harga='+total_harga+'&id_tarif='+id_tarif,
                success: function(data){
                    var obj = jQuery.parseJSON(data);
                    $('#ongkir').val(obj.ongkir);
                    $('#ongkir_hidden').val(obj.ongkir);
                    $('#total').val(obj.total);
                }
            });
        });
    ";
    ?>
</table>
<form action="index.php?halaman=checkout-pesan-barang" method="post">
    <label for="">Kota Tujuan & Kurir</label>
    <select name="id_tarif" id="id_tarif" class="form-control" required>
        <option value=""></option>
        <?php
        // ambil data kota tujuan dan kurir dari database
        $q = mysqli_query($conn, "SELECT *,tarif.id id_tarif FROM tarif
            JOIN kota ON kota.id = tarif.id_kota
            JOIN kurir ON kurir.id = tarif.id_kurir
        ");
        while($d = mysqli_fetch_array($q)) {
            echo "<option value='$d[id_tarif]'>$d[nama_kota] - $d[nama_kurir] (".format_rupiah($d["biaya"])." / kg)</option>";
        }
        ?>
    </select>
    <br>
    <label for="">Total Ongkir (Rupiah)</label>
    <input type="text" name="ongkir" id="ongkir" class="form-control" value="" readonly>
    <input type="hidden" name="ongkir_hidden" id="ongkir_hidden">
    <br>
    <label for="">Total Bayar (Rupiah)</label>
    <input type="text" name="total" id="total" class="form-control" value="" readonly>
    <br>
    <label for="">Alamat Tujuan & Nomor Telepon</label>
    <textarea name="alamat_kirim" id="" cols="30" rows="10" class="form-control" required></textarea>
    <br>
    <input type="submit" name="submit" value="Pesan Barang" class="btn btn-success btn-block">
</form>
