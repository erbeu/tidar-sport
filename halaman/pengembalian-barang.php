<?php
// cek jika tombol kembalikan di submit
if(isset($_POST["submit1"])) {
    // simpan pengembalian barang
    $q = mysqli_query($conn, "INSERT INTO pengembalian_barang VALUES(
        null,
        '$_GET[id]',
        '$_POST[alasan]',
        '".date("Y-m-d H:i:s")."'
        )");

    // reload halaman dan beri pesan bahwa pengembalian akan diproses
    header("location:index.php?halaman=pengembalian-barang&msg=Terimakasih, pengembalian anda akan segera kami proses");
}
?>

<h3>Pengembalian Barang</h3>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<form action="" method="get">
    <input type="hidden" name="halaman" value="pengembalian-barang">
    <label for="">No Pemesanan</label>
    <div class="input-group">
        <span class="input-group-addon" id="id">P-</span>
        <input type="number" name="id" class="form-control" aria-describedby="id" required>
    </div><br>
    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
</form>
<hr>
<?php
// cek jika tombol cek nomor di submit
if(isset($_GET["submit"])) {
    // ambil file cetak nota
    require_once("checkout-cetak-nota.php");

    // cek apakah proses pemesanan sudah selesai atau belum
    if($id_status != 4) {
        // jika belum selesai tampilkan error
        $q1 = mysqli_query($conn, "SELECT * FROM status WHERE id = $id_status");
        $d1 = mysqli_fetch_array($q1);
        echo "<div class='alert alert-danger'>Maaf, anda tidak dapat melakukan pengembalian barang karena status pemesanan <kbd>$d1[nama_status]</kbd>.</div>";
    } else {
        //jika sudah selesai hitung waktu sampai barang dengan hari ini
        $waktu_sampai = new DateTime($waktu_sampai);
        $sekarang = new DateTime();
        $interval = $waktu_sampai->diff($sekarang);
        $interval = $interval->format('%r%a');

        // cek apakah interval waktu sampai barang dengan hari ini melebihi batas waktu pengembalian
        if($interval > $config["batas_pengembalian"]) {
            // jika melebihi waktu tampilkan error
            echo "<div class='alert alert-danger'>Maaf, anda tidak dapat melakukan pengembalian barang karena sudah melebihi waktu yang sudah ditentukan.</div>";
        } else {
            // jika tidak tampilkan form
            ?>
            <hr>
            <form action="" method="post" enctype="multipart/form-data">
                <label for="">Alasan Pengembalian</label>
                <textarea name="alasan" rows="10" class="form-control" required></textarea><br>
                <input type="submit" name="submit1" value="Kembalikan Barang" class="btn btn-primary">
            </form>
            <?php
        }
    }
}
?>
