<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<div class="jumbotron">
    <h1>Selamat Datang Juragan,..</h1>
    <h3>" Disini tempat agan mencari barang yang BERKUALITAS dan mutu yang BAIK serta TERPERCAYA..."</h3>
    <p><a class="btn btn-primary btn-lg" href="index.php?halaman=produk" role="button">Lihat Produk &raquo;</a></p>
</div>
<hr>
<h3>Produk pilihan untuk anda</h3>
<hr>
<div class="row produk">
    <?php
    // ambil 3 produk secara acak
    $q = mysqli_query($conn, "SELECT * FROM produk WHERE stok > 0 ORDER BY RAND() LIMIT 3");
    
    // looping
    while($d = mysqli_fetch_array($q)) {
        // tampilkan data produk
        echo "
            <div class='col-md-4 produk-box'>
                <img src='assets/img/produk/$d[gambar]' class='img-responsive gambar'>
                <h3>$d[nama_produk]</h3>
                <p>".substr($d["deskripsi"], 0, 50)." ..</p>
                <p>".format_rupiah($d["harga"])."</p>
                <a class='btn btn-primary btn-block' href='index.php?halaman=produk-detail&id=$d[id]' role='button'>Detail &raquo;</a>
            </div>
        ";
    }
    ?>
</div>