<?php
// cek jika tombol disubmit
if(isset($_POST["submit"])) {
    // simpan pesan masuk
    $q = mysqli_query($conn, "INSERT INTO pesan_masuk VALUES(
        null,
        '$_POST[nama]',
        '$_POST[email]',
        '$_POST[subjek]',
        '$_POST[isi]',
        '".date("Y-m-d")."'
        )");
    // reload halaman dan beri pesan bahwa pesan dikirim
    header("location:index.php?halaman=hubungi-kami&msg=Terimakasih, Pesan Anda Sudah Terkirim");
}
?>

<h3>Hubungi Kami</h3>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<form action="" method="post">
    <label for="">Nama</label>
    <input type="text" name="nama" class="form-control" required><br>
    <label for="">Email</label>
    <input type="text" name="email" class="form-control" required><br>
    <label for="">Subjek</label>
    <input type="text" name="subjek" class="form-control" required><br>
    <label for="">Isi</label>
    <textarea name="isi" id="" cols="30" rows="10" class="form-control" required></textarea><br>
    <input type="submit" name="submit" value="Kirim Pesan" class="btn btn-primary">
</form>