<?php
// cek jika tombol disubmit
if(isset($_POST["submit"])) {
    // beri nama, lokasi penyimpanan, dan cek gambar
    $nama_gambar = rand(1,9).md5(rand(1,999));
    $gambar1 = $nama_gambar."-logo.jpg";
    $gambar2 = $nama_gambar."-desain.jpg";
    $target_file1 = "assets/img/custom/$gambar1";
    $target_file2 = "assets/img/custom/$gambar2";
    $check1 = getimagesize($_FILES["gambar"]["tmp_name"][0]);
    $check2 = getimagesize($_FILES["gambar"]["tmp_name"][1]);

    // cek jika gambar berhasil diupload
    if($check1 && $check2 && move_uploaded_file($_FILES["gambar"]["tmp_name"][0], $target_file1) && move_uploaded_file($_FILES["gambar"]["tmp_name"][1], $target_file2)) {
        // ambil data tarif
        $q_tarif = mysqli_query($conn, "SELECT * FROM tarif WHERE id = '$_POST[id_tarif]'");
        $d_tarif = mysqli_fetch_array($q_tarif);

        // simpan custom order
        $_POST["nama_no"] = nl2br($_POST["nama_no"]);
        $q = mysqli_query($conn, "INSERT INTO pemesanan_custom VALUES(
            null,
            '$_SESSION[id_pembeli]',
            '1',
            '$d_tarif[id_kurir]',
            '$d_tarif[id_kota]',
            '$_POST[ongkir_hidden]',
            '$_POST[alamat_kirim]',
            '".date("Y-m-d G:i:s")."',
            null,
            '$_POST[bahan]',
            '$_POST[harga_bahan]',
            '$_POST[ukuran]',
            '$gambar1',
            '$gambar2',
            '$_POST[jumlah]',
            '$_POST[nama_no]',
            '$_POST[ket]'
        )") or die(mysqli_error($conn));
        $id_pemesanan = mysqli_insert_id($conn);

        // reload halaman dan beri pesan bahwa pesanan disimpan
        header("location:index.php?halaman=custom-order-selesai&id=$id_pemesanan&msg=Terimakasih telah berbelanja di TIDAR SPORT");
    } else {
        // jika gagal, beri pesan error
        $msg = "Gagal upload";
    }
}

// ambil data ukuran
$ukuran = "";
foreach(ukuran() as $data) {
    $ukuran .= "<option>$data</option>";
}
$bahan = "";
foreach ($config["bahan_baju"] as $key => $data) {
    $data["harga"] = format_rupiah($data["harga"]);
    $bahan .= "<option>$data[nama]</option>";
}

// ajax untuk mengambil ongkir dan menghitung total harga
$js = "
    $('#harga').number(true);
    $('#ongkir').number(true);
    $('#total').number(true);
    $('#bahan').on('change', function() {
        var nama = $(this).val();
        $.ajax({
            url: 'ajax.php?get=harga_bahan&nama='+nama,
            success: function(data){
                $('#harga_bahan').val(data);
            }
        });
    });
    $('#jumlah').on('change', function() {
        var harga = $('#harga_bahan').val() * $('#jumlah').val();
        $('#harga').val(harga);
    });
    $('#id_tarif').on('change', function() {
        var total_barang = $('#jumlah').val();
        var total_harga = $('#harga').val();
        var id_tarif = $(this).val();
        $.ajax({
            url: 'ajax.php?get=ongkir&total_barang='+total_barang+'&total_harga='+total_harga+'&id_tarif='+id_tarif,
            success: function(data){
                var obj = jQuery.parseJSON(data);
                $('#ongkir').val(obj.ongkir);
                $('#ongkir_hidden').val(obj.ongkir);
                $('#total').val(obj.total);
            }
        });
    });
";
?>

<h3>Custom Order</h3>

<?php
if(empty($_SESSION["id_pembeli"])) {
    echo "<p>Selain menjual produk-produk pabrikan, kami juga menjual produk dengan desain custom. Silahkan login/mendaftar terlebih dahulu untuk membuat custom order.</p>";
} else {
    ?>
    <p>Selain menjual produk-produk pabrikan, kami juga menjual produk dengan desain custom. Silahkan isi form berikut untuk membuat jersey anda sendiri.</p>

    <?php
    // tampilkan pesan jika ada
    echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "<br>";
    ?>

    <form action="" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-4">
                <label for="">Bahan</label>
                <select name='bahan' id="bahan" class='form-control' required>
                    <option value=''>-- Pilih Bahan--</option>
                    <?php echo $bahan; ?>
                </select><br>
                <input type="hidden" name="harga_bahan" id="harga_bahan">
                <label for="">Ukuran</label>
                <select name='ukuran' class='form-control' required>
                    <option value=''>-- Pilih Ukuran--</option>
                    <?php echo $ukuran; ?>
                </select><br>
                <label for="">Logo</label>
                <input type="file" name="gambar[]" class="form-control" required><br>
                <label for="">Desain Baju</label>
                <input type="file" name="gambar[]" class="form-control" required><br>
                <label for="">Jumlah</label>
                <input type="number" id="jumlah" name="jumlah" class="form-control" min="1" required><br>
            </div>
            <div class="col-md-4">
                <label for="">Nama & No Punggung</label>
                <textarea name="nama_no" id="" cols="30" rows="10" class="form-control" required></textarea>
                <p class="help-block">Format isian nama & no punggung:<br>Joko - 1<br>Jaka - 11</p>
            </div>
            <div class="col-md-4">
                <label for="">Keterangan Tambahan</label>
                <textarea name="ket" id="" cols="30" rows="10" class="form-control"></textarea><br>
            </div>
            <div class="col-md-8">

                <label for="">Harga (Rupiah)</label>
                <input type="text" id="harga" name="harga" class="form-control" readonly><br>
            </div>
        </div>
        <hr>
        <label for="">Kota Tujuan & Kurir</label>
        <select name="id_tarif" id="id_tarif" class="form-control" required>
            <option value=""></option>
            <?php
            // ambil data kota tujuan dan kurir dari database
            $q = mysqli_query($conn, "SELECT *,tarif.id id_tarif FROM tarif
                JOIN kota ON kota.id = tarif.id_kota
                JOIN kurir ON kurir.id = tarif.id_kurir
            ");
            while($d = mysqli_fetch_array($q)) {
                echo "<option value='$d[id_tarif]'>$d[nama_kota] - $d[nama_kurir] (".format_rupiah($d["biaya"])." / kg)</option>";
            }
            ?>
        </select>
        <br>
        <label for="">Total Ongkir (Rupiah)</label>
        <input type="text" name="ongkir" id="ongkir" class="form-control" value="" readonly>
        <input type="hidden" name="ongkir_hidden" id="ongkir_hidden">
        <br>
        <label for="">Total Bayar (Rupiah)</label>
        <input type="text" name="total" id="total" class="form-control" value="" readonly>
        <br>
        <label for="">Alamat Tujuan & Nomor Telepon</label>
        <textarea name="alamat_kirim" id="" cols="30" rows="10" class="form-control" required></textarea>
        <br>
        <input type="submit" name="submit" value="Pesan Barang" class="btn btn-primary">
    </form>

    <?php
}
?>
