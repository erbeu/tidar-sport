<?php
// ambil file penting
require_once("config.php");
require_once("fungsi.php");

$get = $_GET["get"];

// cek jika ingin mengambil ongkir
if ($get == "ongkir") {
    // ambil data ongkir
    $q = mysqli_query($conn, "SELECT * FROM tarif WHERE id = '$_GET[id_tarif]'");
    $d = mysqli_fetch_array($q);

    // 1kg = 4 produk
    $kg = ceil($_GET["total_barang"] / 4);

    $ongkir = $d["biaya"] * $kg;
    $data["ongkir"] = $ongkir;
    $data["total"] = $ongkir + $_GET["total_harga"];

    // tampilkan data ongkir
    echo json_encode($data);
} elseif ($get == "harga_bahan") {
    // jika ingin mengambil harga bahan
    $return = 0;
    foreach ($config["bahan_baju"] as $key => $data) {
        if ($data["nama"] == $_GET["nama"]) {
            $return = $data["harga"];
        }
    }

    echo $return;
}
