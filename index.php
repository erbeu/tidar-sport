<?php
// inisiasi
ob_start();
session_start();

// ambil file penting
require_once("config.php");
require_once("fungsi.php");

// ambil nama halaman, jika tidak ada ambil halaman home
$halaman = isset($_GET["halaman"]) ? $_GET["halaman"] : "home";

// ambil nama layout, jika tidak ada ambil layout default
$layout = isset($_GET["layout"]) ? $_GET["layout"] : "default";

// ambil id, jika tidak ada maka kosong
$id = isset($_GET["id"]) ? $_GET["id"] : null;

// ambil pesan, jika tidak ada maka kosong
$msg = isset($_GET["msg"]) ? $_GET["msg"] : null;

// cek file halaman, jika tidak ada maka ambil halaman 404
$file_halaman = file_exists("halaman/".$halaman.".php") ? $halaman : '404';

// cek file layout, jika tidak ada maka ambil layout default
$file_layout = file_exists("layout/".$layout.".php") ? $layout : 'default';

// ambil layout di folder layout
require("layout/$file_layout.php");

ob_end_flush();
mysqli_close($conn);
