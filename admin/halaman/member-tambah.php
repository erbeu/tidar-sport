<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // simpan data member ke tabel pembeli
    $q = mysqli_query($conn, "INSERT INTO pembeli VALUES(
        null,
        '$_POST[nama_pembeli]',
        '$_POST[alamat]',
        '$_POST[email]',
        '$_POST[telepon]',
        '$_POST[username]',
        MD5('$_POST[password]')
        )");

    // alihkan ke halaman member dan beri pesan berhasil
    header("location:?halaman=member&msg=Data Berhasil Disimpan");
}
?>

<h3>Tambah Member</h3>
<hr>
<form action="" method="post">
    <label for="">Nama Lengkap</label>
    <input type="text" name="nama_pembeli" class="form-control" required>
    <br>
    <label for="">Alamat</label>
    <textarea name="alamat" id="" cols="30" rows="10" class="form-control" required></textarea>
    <br>
    <label for="">Email</label>
    <input type="email" name="email" class="form-control" required>
    <br>
    <label for="">No. Telepon</label>
    <input type="text" name="telepon" class="form-control" required>
    <br>
    <label for="">Username</label>
    <input type="text" name="username" class="form-control" required>
    <br>
    <label for="">Password</label>
    <input type="password" name="password" class="form-control" required>
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=member" class="btn btn-default">Batal</a>
</form>
