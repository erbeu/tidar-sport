<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // simpan data admin ke tabel admin
    $q = mysqli_query($conn, "INSERT INTO admin VALUES(
        null,
        '$_POST[nama]',
        '$_POST[username]',
        MD5('$_POST[password]')
    )");
    
    // alihkan ke halaman admin dan beri pesan berhasil
    header("location:?halaman=admin&msg=Data Berhasil Disimpan");
}
?>

<h3>Tambah Admin</h3>
<hr>
<form action="" method="post">
    <label>Nama</label>
    <input type="text" name="nama" class="form-control" required>
    <br>
    <label>Username</label>
    <input type="text" name="username" class="form-control" required>
    <br>
    <label>Password</label>
    <input type="password" name="password" class="form-control" required>
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=admin" class="btn btn-default">Batal</a>
</form>