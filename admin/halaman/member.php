<h3>Member</h3>
<a href="index.php?halaman=member-tambah" class="btn btn-primary">Tambah</a>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>ID Member</th>
            <th>Nama Member</th>
            <th>Username</th>
            <th>Email</th>
            <th>Telepon</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;

        // ambil data semua member
        $q = mysqli_query($conn, "SELECT * FROM pembeli
            ORDER BY id DESC");
        while($d = mysqli_fetch_array($q)) {

            // tampilkan data member
            echo "
                <tr>
                    <td>$no</td>
                    <td>K$d[id]</td>
                    <td>$d[nama_pembeli]</td>
                    <td>$d[username]</td>
                    <td>$d[email]</td>
                    <td>$d[telepon]</td>
                    <td>
                        <a href='index.php?halaman=member-edit&id=$d[id]'>Edit</a> |
                        <a href='index.php?halaman=member-hapus&id=$d[id]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>
