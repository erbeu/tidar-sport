<?php
// ambil data penjualan dari tabel pemesanan, pemesanan detail, pembeli, status, kurir, kota, produk sesuai bulan dan tahun yg dipilih
$q = mysqli_query($conn, "SELECT
    pemesanan.id,
    pemesanan.biaya_kirim,
    pemesanan.waktu,
    pembeli.id id_pembeli,
    pembeli.nama_pembeli,
    pembeli.telepon,
    kota.nama_kota,
    kurir.nama_kurir,
    produk.nama_produk,
    produk.harga,
    produk.warna,
    pemesanan_detail.ukuran,
    pemesanan_detail.stok_awal,
    pemesanan_detail.jumlah,
    status.nama_status
    FROM
    pemesanan
    JOIN pemesanan_detail ON pemesanan_detail.id_pemesanan = pemesanan.id
    JOIN pembeli ON pemesanan.id_pembeli = pembeli.id
    JOIN `status` ON pemesanan.id_status = `status`.id
    JOIN kurir ON pemesanan.id_kurir = kurir.id
    JOIN kota ON pemesanan.id_kota = kota.id
    JOIN produk ON pemesanan_detail.id_produk = produk.id
    WHERE MONTH(pemesanan.waktu) = '$_GET[bulan]'
    AND YEAR(pemesanan.waktu) = '$_GET[tahun]'");

$no = 1;
$last_id = 0;
$jumlah_produk = 0;
$total_harga = 0;
$jumlah_harga = 0;
$diskon = 0;
$biaya_kirim = 0;
$jumlah_dibayar = 0;
?>

<div class="text-center">
    <h3>LAPORAN PENJUALAN TIDAR SPORT<br>
    BULAN <?php echo strtoupper(bulan($_GET["bulan"]))." ".$_GET["tahun"]; ?></h3>
</div>
<hr>
<table class="table table-bordered produk">
    <thead>
        <tr>
            <th>No</th>
            <th>Waktu</th>
            <th>ID Member<br>Nama Pembeli</th>
            <th>No Telp</th>
            <th>Kota</th>
            <th>Kurir</th>
            <th>Nama Produk</th>
            <th>Warna</th>
            <th>Ukuran</th>
            <th>Stok Awal</th>
            <th>Jumlah Pesanan</th>
            <th>Stok Akhir</th>
            <th width="10%">Harga</th>
            <th width="10%">Total Harga</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        // ambil semua data penjualan
        $data = mysqli_fetch_all($q, MYSQLI_ASSOC);

        // looping data penjualan
        foreach($data as $key => $d) {
            // jumlah produk = jumlah produk sebelumnya + jumlah produk yg dipesan
            $jumlah_produk += $d["jumlah"];

            // total harga = harga produk * jumlah produk yg dipesan
            $total_harga = $d["harga"] * $d["jumlah"];

            // jumlah harga = jumlah harga sebelumnya + total harga
            $jumlah_harga += $total_harga;

            // jika id sebelumnya tidak sama dengan id saat ini maka tampilkan seluruh detail
            if($last_id != $d["id"]) {
                // tampilkan data penjualan
                $no_pesanan = 1;
                echo "
                    <tr>
                        <td>$no</td>
                        <td>$d[waktu]</td>
                        <td>K$d[id_pembeli]<br>$d[nama_pembeli]</td>
                        <td>$d[telepon]</td>
                        <td>$d[nama_kota]</td>
                        <td>$d[nama_kurir]</td>
                        <td>$no_pesanan. $d[nama_produk]</td>
                        <td>$d[warna]</td>
                        <td>$d[ukuran]</td>
                        <td>$d[stok_awal]</td>
                        <td>$d[jumlah]</td>
                        <td>".($d["stok_awal"] - $d["jumlah"])."</td>
                        <td>".format_rupiah($d["harga"])."</td>
                        <td>".format_rupiah($total_harga)."</td>
                        <td>$d[nama_status]</td>
                    </tr>
                    ";
                $no++;
                $no_pesanan++;

            // jika id sebelumnya sama dengan id saat ini maka tampilkan sebagian detail
            } else {
                // tampilkan data penjualan
                echo "
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>$no_pesanan. $d[nama_produk]</td>
                        <td>$d[warna]</td>
                        <td>$d[ukuran]</td>
                        <td>$d[stok_awal]</td>
                        <td>$d[jumlah]</td>
                        <td>".($d["stok_awal"] - $d["jumlah"])."</td>
                        <td>".format_rupiah($d["harga"])."</td>
                        <td>".format_rupiah($total_harga)."</td>
                        <td></td>
                    </tr>
                    ";
            }

            // jika data setelahnya kosong atau id setelah ini tidak sama dengan id saat ini maka tampilkan biaya kirim dan sub total
            if(empty($data[$key+1]) || $data[$key+1]["id"] != $d["id"]) {
                $jumlah_dibayar = $jumlah_harga;

                // cek jumlah pembelian apakah melebihi minimum diskon
                if($jumlah_produk >= $config["min_diskon"]) {
                    // diskon = jumlah harga * persen diskon
                    $diskon = $jumlah_harga * $config["persen_diskon"];
                    // jika jumlah melebihi minimum diskon maka harga akan dikurangi diskon
                    $jumlah_dibayar = $jumlah_harga - $diskon;
                }

                // tampilkan jumlah produk, diskon, biaya kirim, dan jumlah yang dibayar
                echo "
                    <tr>
                        <td colspan='11' rowspan='4'></td>
                        <td colspan='2'>Jumlah</td>
                        <td>".format_rupiah($jumlah_harga)."</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan='2'>Diskon / Potongan Harga</td>
                        <td>".format_rupiah($diskon)."</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan='2'>Biaya Kirim</td>
                        <td>".format_rupiah($d["biaya_kirim"])."</td>
                        <td></td>
                    </tr>
                    <tr class='total'>
                        <td colspan='2'>Jumlah yang dibayar</td>
                        <td>".format_rupiah($jumlah_dibayar + $d["biaya_kirim"])."</td>
                        <td></td>
                    </tr>
                ";
                $jumlah_produk = 0;
                $total_harga = 0;
                $jumlah_harga = 0;
                $diskon = 0;
                $biaya_kirim = 0;
                $jumlah_dibayar = 0;
            }
            $last_id = $d["id"];
        }
        ?>
    </tbody>
</table>
