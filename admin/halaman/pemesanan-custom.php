<h3>Pemesanan Custom</h3>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>ID Pemesanan Custom</th>
            <th>Nama Pembeli</th>
            <th>Kurir</th>
            <th>Kota</th>
            <th>Alamat Kirim</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;

        // ambil semua data pemesanan custom dari tabel pemesanan_custom, status, pembeli, kurir, kota
        $q = mysqli_query($conn, "SELECT *, pemesanan_custom.id as id_pemesanan_custom FROM pemesanan_custom
            JOIN status ON status.id = pemesanan_custom.id_status
            JOIN pembeli ON pembeli.id = pemesanan_custom.id_pembeli
            JOIN kurir ON kurir.id = pemesanan_custom.id_kurir
            JOIN kota ON kota.id = pemesanan_custom.id_kota
            ORDER BY pemesanan_custom.id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data pemesanan_custom
            echo "
                <tr>
                    <td>$no</td>
                    <td>C-$d[id_pemesanan_custom]</td>
                    <td>$d[nama_pembeli]</td>
                    <td>$d[nama_kurir]</td>
                    <td>$d[nama_kota]</td>
                    <td>$d[alamat_kirim]</td>
                    <td>$d[nama_status]</td>
                    <td>
                        <a href='index.php?halaman=pemesanan-custom-detail&id=$d[id_pemesanan_custom]'>Detail</a> |
                        <a href='index.php?halaman=pemesanan-custom-hapus&id=$d[id_pemesanan_custom]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>
