<form action="" method="get" target="_blank" class="form-inline">

    <input type="hidden" name="halaman" value="laporan-penjualan-custom-bulanan-print">
    <input type="hidden" name="layout" value="print">

    <select name="bulan" class="form-control" required>
        <option value="">Pilih Bulan</option>

        <?php
        // ambil data semua bulan
        foreach(bulan() as $key => $value) {
            // tampilkan bulan
            echo "<option value=\"$key\">$value</option>";
        }
        ?>

    </select>

    <select name="tahun" class="form-control" required>
        <option value="">Pilih Tahun</option>

        <?php
        // looping dari tahun 2017 sd tahun ini
        for($i = 2017; $i <= date("Y"); $i++) {
            // tampilkan tahun
            echo "<option>$i</option>";
        }
        ?>

    </select>

    <button type="submit" class="btn btn-primary">Lihat Laporan</button>

</form>
