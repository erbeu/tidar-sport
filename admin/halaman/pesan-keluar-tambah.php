<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // kirim email ke admin@tidarsport.com
    $headers = "From: admin@tidarsport.com";
    mail($_POST[email_tujuan],$_POST[subjek],$_POST[isi],$headers);
    
    // simpan data pesan keluar
    $q = mysqli_query($conn, "INSERT INTO pesan_keluar VALUES(
        null,
        '$_POST[email_tujuan]',
        '$_POST[subjek]',
        '$_POST[isi]',
        '".date("Y-m-d")."'
    )");
    
    // alihkan ke halaman pesan keluar dan beri pesan berhasil
    header("location:index.php?halaman=pesan-keluar&msg=Pesan Berhasil Dikirim");
}
?>

<h3>Tambah Pesan Keluar</h3>
<hr>
<form action="" method="post">
    <label>Email Tujuan</label>
    <input type="text" name="email_tujuan" class="form-control" required>
    <br>
    <label>Subjek</label>
    <input type="text" name="subjek" class="form-control" required>
    <br>
    <label>Isi</label>
    <textarea name="isi" class="form-control" required></textarea>
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=pesan-keluar" class="btn btn-default">Batal</a>
</form>