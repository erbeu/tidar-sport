<h3>Pesan Keluar</h3>
<a href="index.php?halaman=pesan-keluar-tambah" class="btn btn-primary">Tambah</a>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Email Tujuan</th>
            <th>Subjek</th>
            <th>Isi</th>
            <th>Tanggal</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        
        // ambil data semua pesan keluar
        $q = mysqli_query($conn, "SELECT * FROM pesan_keluar ORDER BY id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data pesan keluar
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[email_tujuan]</td>
                    <td>$d[subjek]</td>
                    <td>$d[isi]</td>
                    <td>$d[tanggal]</td>
                    <td>
                        <a href='index.php?halaman=pesan-keluar-hapus&id=$d[id]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>