<h3>Konfirmasi Pembayaran Custom Order</h3>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Pembeli</th>
            <th width='100px'>Bukti</th>
            <th>Waktu</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;

        // ambil data semua konfirmasi pembayaran custom
        $q = mysqli_query($conn, "SELECT
            konfirmasi_pembayaran_custom.id,
            konfirmasi_pembayaran_custom.id_pemesanan_custom,
            pembeli.nama_pembeli,
            konfirmasi_pembayaran_custom.foto,
            konfirmasi_pembayaran_custom.waktu
            FROM
            konfirmasi_pembayaran_custom
            INNER JOIN pemesanan_custom ON pemesanan_custom.id = konfirmasi_pembayaran_custom.id_pemesanan_custom
            INNER JOIN pembeli ON pembeli.id = pemesanan_custom.id_pembeli
            ORDER BY pemesanan_custom.id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data konfirmasi pembayaran custom
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[nama_pembeli]</td>
                    <td><img src='../assets/img/konfirmasi/$d[foto]' width='100px' height='100px' class='img-thumbnail'></td>
                    <td>$d[waktu]</td>
                    <td>
                        <a href='index.php?halaman=konfirmasi-pembayaran-custom-detail&id=$d[id]'>Detail</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>
