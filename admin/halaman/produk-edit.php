<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // cek jika gambar diupload
    if(!empty($_FILES["gambar"]["tmp_name"])) {
        // jika diupload, beri nama, lokasi penyimpanan, dan cek gambar
        $gambar = "$_POST[nama_produk].jpg";
        $target_file = "../assets/img/produk/$gambar";
        $check = getimagesize($_FILES["gambar"]["tmp_name"]);
    
        // cek jika gambar berhasil diupload
        if($check && move_uploaded_file($_FILES["gambar"]["tmp_name"], $target_file)) {
            // jika berhasil, update semua field di tabel produk
            $q = mysqli_query($conn, "UPDATE produk SET
                nama_produk = '$_POST[nama_produk]',
                deskripsi = '$_POST[deskripsi]',
                warna = '$_POST[warna]',
                ukuran = '$_POST[ukuran]',
                harga = '$_POST[harga]',
                stok = '$_POST[stok]',
                gambar = '$gambar'
                WHERE id = '$id'
            ");
            
            // alihkan ke halaman produk dan beri pesan berhasil
            header("location:index.php?halaman=produk&msg=Data Berhasil Disimpan");
        } else {
            // jika gagal, beri pesan error
            $msg = "Gagal upload";
        }
    } else {
        // jika tidak diupload, update semua field di tabel produk kecuali gambar
        $q = mysqli_query($conn, "UPDATE produk SET
            nama_produk = '$_POST[nama_produk]',
            deskripsi = '$_POST[deskripsi]',
            warna = '$_POST[warna]',
            harga = '$_POST[harga]',
            stok = '$_POST[stok]'
            WHERE id = '$id'
        ");
        
        // alihkan ke halaman produk dan beri pesan berhasil
        header("location:index.php?halaman=produk&msg=Data Berhasil Disimpan");
    }
}

// ambil data produk sesuai id
$q = mysqli_query($conn, "SELECT * FROM produk WHERE id = '$id'");
$d = mysqli_fetch_array($q);
?>

<h3>Edit Produk</h3>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-danger'>$msg</div>" : "";
?>

<form action="" method="post" enctype="multipart/form-data">
    <label>Nama Produk</label>
    <input type="text" name="nama_produk" class="form-control" value="<?php echo $d["nama_produk"] ?>" required>
    <br>
    <label>Deskripsi</label>
    <textarea name="deskripsi" class="form-control" required><?php echo $d["deskripsi"] ?></textarea>
    <br>
    <label>Warna</label>
    <input type="text" name="warna" class="form-control" value="<?php echo $d["warna"] ?>" required>
    <br>
    <label>Harga</label>
    <input type="text" name="harga" class="form-control" value="<?php echo $d["harga"] ?>" required>
    <br>
    <label>Stok</label>
    <input type="text" name="stok" class="form-control" value="<?php echo $d["stok"] ?>" required>
    <br>
    <label>Gambar</label>
    <input type="file" name="gambar" class="form-control">
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=produk" class="btn btn-default">Batal</a>
</form>