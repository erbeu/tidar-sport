<?php
// cek jika tombol submit ditekan
if(isset($_POST["submit"])) {
    // beri nama, lokasi penyimpanan, dan cek gambar
    $gambar = "$_POST[nama_produk].jpg";
    $target_file = "../assets/img/produk/$gambar";
    $check = getimagesize($_FILES["gambar"]["tmp_name"]);
    
    // cek jika gambar berhasil diupload
    if($check && move_uploaded_file($_FILES["gambar"]["tmp_name"], $target_file)) {
        // jika berhasil, simpan data di tabel produk
        $q = mysqli_query($conn, "INSERT INTO produk VALUES(
            null,
            '$_POST[nama_produk]',
            '$_POST[deskripsi]',
            '$_POST[warna]',
            '$_POST[harga]',
            '$_POST[stok]',
            '$gambar'
        )");
        
        // alihkan ke halaman produk dan beri pesan berhasil
        header("location:index.php?halaman=produk&msg=Data Berhasil Disimpan");
    } else {
        // jika gagal, beri pesan error
        $msg = "Gagal upload";
    }
}
?>

<h3>Tambah Produk</h3>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-danger'>$msg</div>" : "";
?>

<form action="" method="post" enctype="multipart/form-data">
    <label>Nama Produk</label>
    <input type="text" name="nama_produk" class="form-control" required>
    <br>
    <label>Deskripsi</label>
    <textarea name="deskripsi" class="form-control" required></textarea>
    <br>
    <label>Warna</label>
    <input type="text" name="warna" class="form-control" required>
    <br>
    <label>Harga</label>
    <input type="text" name="harga" class="form-control" required>
    <br>
    <label>Stok</label>
    <input type="text" name="stok" class="form-control" required>
    <br>
    <label>Gambar</label>
    <input type="file" name="gambar" class="form-control" required>
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=produk" class="btn btn-default">Batal</a>
</form>