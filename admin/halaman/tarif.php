<h3>Tarif</h3>
<a href="index.php?halaman=tarif-tambah" class="btn btn-primary">Tambah</a>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Kurir</th>
            <th>Nama Kota</th>
            <th>Biaya</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        
        // ambil data semua tarif dari tabel tarif, kurir, kota
        $q = mysqli_query($conn, "SELECT * FROM tarif
            JOIN kurir ON kurir.id = tarif.id_kurir
            JOIN kota ON kota.id = tarif.id_kota
            ORDER BY tarif.id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data tarif
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[nama_kurir]</td>
                    <td>$d[nama_kota]</td>
                    <td>$d[biaya]</td>
                    <td>
                        <a href='index.php?halaman=tarif-edit&id=$d[id]'>Edit</a> |
                        <a href='index.php?halaman=tarif-hapus&id=$d[id]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>