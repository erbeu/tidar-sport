<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // update tabel tarif
    $q = mysqli_query($conn, "UPDATE tarif SET
        id_kurir = '$_POST[id_kurir]',
        id_kota = '$_POST[id_kota]',
        biaya = '$_POST[biaya]'
        WHERE id = $id
    ");
    
    // alihkan ke halaman tarif dan beri pesan berhasil
    header("location:index.php?halaman=tarif&msg=Data Berhasil Disimpan");
}

// ambil data tarif sesuai id
$q = mysqli_query($conn, "SELECT * FROM tarif WHERE id = '$id'");
$d = mysqli_fetch_array($q);
?>

<h3>Edit Tarif</h3>
<hr>
<form action="" method="post">
    <label>Nama Kurir</label>
    <select name="id_kurir" class="form-control" required>
        <option value=""></option>
        <?php
        // ambil data semua kurir
        $q1 = mysqli_query($conn, "SELECT * FROM kurir");
        while($d1 = mysqli_fetch_array($q1)) {
            // tampilkan data kurir
            $select = $d["id_kurir"] == $d1["id"] ? "selected" : "";
            echo "<option value='$d1[id]' $select>$d1[nama_kurir]</option>";
        }
        ?>
    </select>
    <br>
    <label>Nama Kota</label>
    <select name="id_kota" class="form-control" required>
        <option value=""></option>
        <?php
        // ambil data semua kota
        $q2 = mysqli_query($conn, "SELECT * FROM kota");
        while($d2 = mysqli_fetch_array($q2)) {
            // tampilkan data kota
            $select = $d["id_kota"] == $d2["id"] ? "selected" : "";
            echo "<option value='$d2[id]' $select>$d2[nama_kota]</option>";
        }
        ?>
    </select>
    <br>
    <label>Biaya</label>
    <input type="text" name="biaya" class="form-control" value="<?php echo $d["biaya"] ?>" required>
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=tarif" class="btn btn-default">Batal</a>
</form>