<?php
// ambil data penjualan dari tabel pemesanan_custom, pembeli, status, kurir, dan kota sesuai bulan dan tahun yg dipilih
$q = mysqli_query($conn, "SELECT *, pemesanan_custom.id as id_pemesanan_custom
    FROM pemesanan_custom
    JOIN status ON status.id = pemesanan_custom.id_status
    JOIN pembeli ON pembeli.id = pemesanan_custom.id_pembeli
    JOIN kurir ON kurir.id = pemesanan_custom.id_kurir
    JOIN kota ON kota.id = pemesanan_custom.id_kota
    WHERE MONTH(pemesanan_custom.waktu) = '$_GET[bulan]'
    AND YEAR(pemesanan_custom.waktu) = '$_GET[tahun]'");

$no = 1;
$total_harga = 0;
$jumlah_harga = 0;
?>

<div class="text-center">
    <h3>LAPORAN PENJUALAN CUSTOM TIDAR SPORT<br>
    BULAN <?php echo strtoupper(bulan($_GET["bulan"]))." ".$_GET["tahun"]; ?></h3>
</div>
<hr>
<table class="table table-bordered produk">
    <thead>
        <tr>
            <th>No</th>
            <th>Waktu</th>
            <th>ID Member<br>Nama Pembeli</th>
            <th>No Telp</th>
            <th>Kota</th>
            <th>Kurir</th>
            <th>Bahan</th>
            <th>Harga Bahan</th>
            <th>Ukuran</th>
            <th>Logo</th>
            <th>Desain Baju</th>
            <th>Jumlah</th>
            <th>Nama & No Punggung</th>
            <th>Ket</th>
            <th>Biaya Kirim</th>
            <th>Total Harga</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        // ambil semua data penjualan
        $data = mysqli_fetch_all($q, MYSQLI_ASSOC);

        // looping data penjualan
        foreach($data as $key => $d) {
            // total harga = harga produk * jumlah produk yg dipesan
            $total_harga = $d["harga_bahan"] * $d["jumlah"] + $d["biaya_kirim"];

            // jumlah harga = jumlah harga sebelumnya + total harga
            $jumlah_harga += $total_harga;

            // tampilkan data penjualan
            $no_pesanan = 1;
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[waktu]</td>
                    <td>K$d[id_pembeli]<br>$d[nama_pembeli]</td>
                    <td>$d[telepon]</td>
                    <td>$d[nama_kota]</td>
                    <td>$d[nama_kurir]</td>
                    <td>$d[bahan]</td>
                    <td>".format_rupiah($d["harga_bahan"])."</td>
                    <td>$d[ukuran]</td>
                    <td><img src='../assets/img/custom/$d[logo]'></td>
                    <td><img src='../assets/img/custom/$d[desain_baju]'></td>
                    <td>$d[jumlah]</td>
                    <td>$d[nama_no]</td>
                    <td>$d[ket]</td>
                    <td>".format_rupiah($d["biaya_kirim"])."</td>
                    <td>".format_rupiah($total_harga)."</td>
                    <td>$d[nama_status]</td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>
