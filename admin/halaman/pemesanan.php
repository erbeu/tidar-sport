<h3>Pemesanan</h3>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>ID Pemesanan</th>
            <th>Nama Pembeli</th>
            <th>Kurir</th>
            <th>Kota</th>
            <th>Alamat Kirim</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;

        // ambil semua data pemesanan dari tabel pemesanan, status, pembeli, kurir, kota
        $q = mysqli_query($conn, "SELECT *, pemesanan.id as id_pemesanan FROM pemesanan
            JOIN status ON status.id = pemesanan.id_status
            JOIN pembeli ON pembeli.id = pemesanan.id_pembeli
            JOIN kurir ON kurir.id = pemesanan.id_kurir
            JOIN kota ON kota.id = pemesanan.id_kota
            ORDER BY pemesanan.id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data pemesanan
            echo "
                <tr>
                    <td>$no</td>
                    <td>P-$d[id_pemesanan]</td>
                    <td>$d[nama_pembeli]</td>
                    <td>$d[nama_kurir]</td>
                    <td>$d[nama_kota]</td>
                    <td>$d[alamat_kirim]</td>
                    <td>$d[nama_status]</td>
                    <td>
                        <a href='index.php?halaman=pemesanan-detail&id=$d[id_pemesanan]'>Detail</a> |
                        <a href='index.php?halaman=pemesanan-hapus&id=$d[id_pemesanan]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>
