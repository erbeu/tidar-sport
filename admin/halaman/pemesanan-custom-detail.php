<?php
// cek jika tombol kirim bukti di submit
if(isset($_POST["submit"])) {
    // simpan perubahan status pemesanan custom
    $q = mysqli_query($conn, "UPDATE pemesanan_custom SET id_status = '$_POST[id_status]' WHERE id = '$id'");

    // reload halaman dan beri pesan bahwa status berhasil diubah
    header("location:index.php?halaman=pemesanan-custom-detail&id=$id&msg=Status pemesanan berhasil diubah");
}

// ambil data pemesanan custom sesuai dengan id
$q = mysqli_query($conn, "SELECT *, pemesanan_custom.id as id_pemesanan_custom FROM pemesanan_custom
    JOIN status ON status.id = pemesanan_custom.id_status
    JOIN pembeli ON pembeli.id = pemesanan_custom.id_pembeli
    JOIN kurir ON kurir.id = pemesanan_custom.id_kurir
    JOIN kota ON kota.id = pemesanan_custom.id_kota
    where pemesanan_custom.id = $id");
$d = mysqli_fetch_array($q);
$biaya_kirim = $d["biaya_kirim"];
?>

<h3>Detail Pemesanan Custom</h3>
<hr>

<?php echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : ""; ?>

<table class="table table-bordered">
    <tr>
        <td width="30%">ID Pemesanan</td>
        <td>C-<?php echo $d["id_pemesanan_custom"]; ?></td>
    </tr>
    <tr>
        <td width="30%">Nama Pembeli</td>
        <td><?php echo $d["nama_pembeli"]; ?></td>
    </tr>
    <tr>
        <td>Kurir</td>
        <td><?php echo $d["nama_kurir"]; ?></td>
    </tr>
    <tr>
        <td>Kota</td>
        <td><?php echo $d["nama_kota"]; ?></td>
    </tr>
    <tr>
        <td>Biaya Kirim</td>
        <td><?php echo format_rupiah($biaya_kirim); ?></td>
    </tr>
    <tr>
        <td>Alamat Kirim</td>
        <td><?php echo $d["alamat_kirim"]; ?></td>
    </tr>
    <tr>
        <td>Waktu</td>
        <td><?php echo $d["waktu"]; ?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td>
            <form action="" method="post" class="form-inline">
                <select name="id_status" class="form-control">
                    <?php
                    $q1 = mysqli_query($conn, "SELECT * FROM status");
                    while($d1 = mysqli_fetch_array($q1)) {
                        if($d['id_status'] == $d1['id']) {
                            echo "<option value='$d1[id]' selected>$d1[nama_status]</option>";
                        } else {
                            echo "<option value='$d1[id]'>$d1[nama_status]</option>";
                        }
                    }
                    ?>
                </select>
                <input type="submit" name="submit" value="Ubah Status" class="btn btn-primary">
            </form>
        </td>
    </tr>
</table>
<hr>
<table class="table table-bordered">
    <tr>
        <td width="30%">Bahan</td>
        <td><?php echo $d["bahan"]; ?></td>
    </tr>
    <tr>
        <td>Harga Bahan</td>
        <td><?php echo format_rupiah($d["harga_bahan"]); ?></td>
    </tr>
    <tr>
        <td>Ukuran</td>
        <td><?php echo $d["ukuran"]; ?></td>
    </tr>
    <tr>
        <td>Logo</td>
        <td><img src="../assets/img/custom/<?php echo $d["logo"]; ?>" alt=""></td>
    </tr>
    <tr>
        <td>Desain Baju</td>
        <td><img src="../assets/img/custom/<?php echo $d["desain_baju"]; ?>" alt=""></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td><?php echo $d["jumlah"]; ?></td>
    </tr>
    <tr>
        <td>Nama & No Punggung</td>
        <td><?php echo $d["nama_no"]; ?></td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td><?php echo $d["ket"]; ?></td>
    </tr>
    <tr>
        <td>Total Bayar</td>
        <td><?php echo format_rupiah($d["harga_bahan"]*$d["jumlah"]+$biaya_kirim) ?></td>
    </tr>
</table>
