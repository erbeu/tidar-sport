<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // simpan data kurir di tabel kurir
    $q = mysqli_query($conn, "INSERT INTO kurir VALUES(
        null,
        '$_POST[nama_kurir]'
    )");
    
    // alihkan ke halaman kurir dan beri pesan berhasil
    header("location:index.php?halaman=kurir&msg=Data Berhasil Disimpan");
}
?>

<h3>Tambah Kurir</h3>
<hr>
<form action="" method="post">
    <label>Nama Kurir</label>
    <input type="text" name="nama_kurir" class="form-control" required>
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=kurir" class="btn btn-default">Batal</a>
</form>