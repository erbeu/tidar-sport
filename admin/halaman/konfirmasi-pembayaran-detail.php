<?php
// ambil data konfirmasi pembayaran sesuai id
$q = mysqli_query($conn, "SELECT * FROM konfirmasi_pembayaran
    WHERE id = $id");
$d = mysqli_fetch_array($q);
?>

<h3>Detail Konfirmasi Pembayaran</h3>
<hr>

<table class="table table-bordered">
    <tr>
        <td width="30%">Waktu Konfirmasi</td>
        <td><?php echo $d["waktu"]; ?></td>
    </tr>
    <tr>
        <td>Bukti Pembayaran</td>
        <td><img src="../assets/img/konfirmasi/<?php echo $d["foto"]; ?>" style="height: 300px;"></td>
    </tr>
</table>
<hr>

<?php
$id = $d["id_pemesanan"];
// panggil halaman pemesanan detail
require_once("pemesanan-detail.php");
?>