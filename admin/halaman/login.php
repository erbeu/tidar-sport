<?php
// cek jika tombol login ditekan
if(isset($_POST["submit"])) {
    // ambil data admin sesuai username & password
    $q = mysqli_query($conn, "SELECT * FROM admin
        WHERE
        username = '".$_POST["username"]."' AND
        password = '".md5($_POST["password"])."'");

    // cek jika data tersebut ditemukan
    if(mysqli_num_rows($q) == 1) {
        // jika ada, ambil data admin
        $d = mysqli_fetch_assoc($q);

        // simpan data admin ke session
        $_SESSION["id_admin"] = $d["id"];
        $_SESSION["nama"] = $d["nama"];
        $_SESSION["username"] = $d["username"];

        // alihkan ke halaman utama
        header("location:index.php");
    } else {
        // jika tidak, beri pesan error
        $error = "Username / Password Salah";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<title>LOGIN</title>
<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #204d74;
        color: #fff;
    }

    .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }

    .form-signin .form-signin-heading {
        margin-bottom: 10px;
    }

    .form-signin .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
    }

    .form-signin .form-control:focus {
        z-index: 2;
    }

    .form-signin input[type="text"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }

    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
</style>
</head>
<body>
<div class="container text-center">
    <img src="../assets/img/logo.png" alt="" width="100px">

    <form class="form-signin" action="" method="post">
        <h2 class="form-signin-heading">Silahkan login</h2>

        <?php
        // tampilkan error jika ada
        echo isset($error) ? "<div class=\"alert alert-danger\" role=\"alert\">".$error."</div>" : "";
        ?>

        <label for="username" class="sr-only">Username</label>
        <input type="text" name="username" id="username" class="form-control" placeholder="Username" required autofocus>
        <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-default btn-block" type="submit" name="submit">Log in</button>
    </form>

</div>
</body>

</html>
