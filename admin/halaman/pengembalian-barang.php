<h3>Pengembalian Barang</h3>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>ID Pemesanan</th>
            <th>Nama Pembeli</th>
            <th width='100px'>Alasan</th>
            <th>Waktu</th>
            <th>Status Pemesanan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;

        // ambil data semua pengembalian barang
        $q = mysqli_query($conn, "SELECT
            pengembalian_barang.id,
            pengembalian_barang.id_pemesanan,
            pembeli.nama_pembeli,
            pengembalian_barang.alasan,
            pengembalian_barang.waktu,
            status.nama_status
            FROM
            pengembalian_barang
            INNER JOIN pemesanan ON pemesanan.id = pengembalian_barang.id_pemesanan
            INNER JOIN status ON status.id = pemesanan.id_status
            INNER JOIN pembeli ON pembeli.id = pemesanan.id_pembeli

            ORDER BY pemesanan.id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data pengembalian barang
            echo "
                <tr>
                    <td>$no</td>
                    <td>P-$d[id_pemesanan]</td>
                    <td>$d[nama_pembeli]</td>
                    <td>".substr($d["alasan"], 0, 10)." ...</td>
                    <td>$d[waktu]</td>
                    <td>$d[nama_status]</td>
                    <td>
                        <a href='index.php?halaman=pengembalian-barang-detail&id=$d[id]'>Detail</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>
