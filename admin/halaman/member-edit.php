<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // cek jika password diisi/tidak
    if($_POST["password"] != "") {
        // jika diisi, update semua field tabel pembeli
        $q = mysqli_query($conn, "UPDATE pembeli SET
            nama_pembeli = '$_POST[nama_pembeli]',
            alamat = '$_POST[alamat]',
            email = '$_POST[email]',
            telepon = '$_POST[telepon]',
            username = '$_POST[username]',
            password = MD5('$_POST[password]')
            WHERE id = '$id'
        ");
    } else {
        // jika password kosong, update tabel pembeli kecuali field password
        $q = mysqli_query($conn, "UPDATE pembeli SET
            nama_pembeli = '$_POST[nama_pembeli]',
            alamat = '$_POST[alamat]',
            email = '$_POST[email]',
            telepon = '$_POST[telepon]',
            username = '$_POST[username]'
            WHERE id = '$id'
        ");
    }

    // alihkan ke halaman member dan beri pesan berhasil
    header("location:?halaman=member&msg=Data Berhasil Disimpan");
}

// ambil data member sesuai id
$q = mysqli_query($conn, "SELECT * FROM pembeli WHERE id = '$id'");
$d = mysqli_fetch_array($q);
?>

<h3>Edit Admin</h3>
<hr>
<form action="" method="post">
    <label>Nama</label>
    <label for="">Nama Lengkap</label>
    <input type="text" name="nama_pembeli" class="form-control" value="<?php echo $d["nama_pembeli"] ?>" required>
    <br>
    <label for="">Alamat</label>
    <textarea name="alamat" id="" cols="30" rows="10" class="form-control" required><?php echo $d["alamat"] ?></textarea>
    <br>
    <label for="">Email</label>
    <input type="email" name="email" class="form-control" value="<?php echo $d["email"] ?>" required>
    <br>
    <label for="">No. Telepon</label>
    <input type="text" name="telepon" class="form-control" value="<?php echo $d["telepon"] ?>" required>
    <br>
    <label for="">Username</label>
    <input type="text" name="username" class="form-control" value="<?php echo $d["username"] ?>" required>
    <br>
    <label for="">Password</label>
    <input type="password" name="password" class="form-control">
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=member" class="btn btn-default">Batal</a>
</form>
