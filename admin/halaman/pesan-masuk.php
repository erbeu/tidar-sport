<h3>Pesan Masuk</h3>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email Masuk</th>
            <th>Subjek</th>
            <th>Isi</th>
            <th>Tanggal</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        
        // ambil data semua pesan masuk
        $q = mysqli_query($conn, "SELECT * FROM pesan_masuk ORDER BY id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data pesan masuk
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[nama]</td>
                    <td>$d[email_masuk]</td>
                    <td>$d[subjek]</td>
                    <td>$d[isi]</td>
                    <td>$d[tanggal]</td>
                    <td>
                        <a href='index.php?halaman=pesan-masuk-balas&id=$d[id]'>Balas</a> |
                        <a href='index.php?halaman=pesan-masuk-hapus&id=$d[id]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>