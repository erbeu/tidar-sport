<h3>Produk</h3>
<a href="index.php?halaman=produk-tambah" class="btn btn-primary">Tambah</a>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Produk</th>
            <th>Deskripsi</th>
            <th>Warna</th>
            <th>Harga</th>
            <th>Stok</th>
            <th>Gambar</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        
        // ambil data semua produk
        $q = mysqli_query($conn, "SELECT * FROM produk ORDER BY id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data produk
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[nama_produk]</td>
                    <td>$d[deskripsi]</td>
                    <td>$d[warna]</td>
                    <td>$d[harga]</td>
                    <td>$d[stok]</td>
                    <td>$d[gambar]</td>
                    <td>
                        <a href='index.php?halaman=produk-edit&id=$d[id]'>Edit</a> |
                        <a href='index.php?halaman=produk-hapus&id=$d[id]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>