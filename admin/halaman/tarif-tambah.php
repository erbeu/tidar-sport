<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // simpan data tarif di tabel tarif
    $q = mysqli_query($conn, "INSERT INTO tarif VALUES(
        null,
        '$_POST[id_kurir]',
        '$_POST[id_kota]',
        '$_POST[biaya]'
    )");
    
    // alihkan ke halaman tarif dan beri pesan berhasil
    header("location:index.php?halaman=tarif&msg=Data Berhasil Disimpan");
}
?>

<h3>Tambah Tarif</h3>
<hr>
<form action="" method="post">
    <label>Nama Kurir</label>
    <select name="id_kurir" class="form-control" required>
        <option value=""></option>
        <?php
        // ambil data semua kurir
        $q1 = mysqli_query($conn, "SELECT * FROM kurir");
        while($d1 = mysqli_fetch_array($q1)) {
            // tampilkan data kurir
            echo "<option value='$d1[id]'>$d1[nama_kurir]</option>";
        }
        ?>
    </select>
    <br>
    <label>Nama Kota</label>
    <select name="id_kota" class="form-control" required>
        <option value=""></option>
        <?php
        // ambil data semua kota
        $q2 = mysqli_query($conn, "SELECT * FROM kota");
        while($d2 = mysqli_fetch_array($q2)) {
            // tampilkan data kota
            echo "<option value='$d2[id]'>$d2[nama_kota]</option>";
        }
        ?>
    </select>
    <br>
    <label>Biaya</label>
    <input type="text" name="biaya" class="form-control" required>
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=tarif" class="btn btn-default">Batal</a>
</form>