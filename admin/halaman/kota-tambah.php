<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // simpan data kota di tabel kota
    $q = mysqli_query($conn, "INSERT INTO kota VALUES(
        null,
        '$_POST[nama_kota]'
    )");
    
    // alihkan ke halaman kota dan beri pesan berhasil
    header("location:index.php?halaman=kota&msg=Data Berhasil Disimpan");
}
?>

<h3>Tambah Kota</h3>
<hr>
<form action="" method="post">
    <label>Nama Kota</label>
    <input type="text" name="nama_kota" class="form-control" required>
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=kota" class="btn btn-default">Batal</a>
</form>