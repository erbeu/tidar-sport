<div class="jumbotron welcome text-center">
    <img src="../assets/img/logo.png" alt="" class="logo center-block">
    <h3 class="brand">Aplikasi E-Commerce</h3>
    <small>TIDAR SPORT</small>
    <hr>
    <p>Selamat datang, <strong><?php echo $_SESSION["nama"] ?></strong>. Silahkan pilih menu diatas untuk mengelola aplikasi.</p>
</div>
