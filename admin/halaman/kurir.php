<h3>Kurir</h3>
<a href="index.php?halaman=kurir-tambah" class="btn btn-primary">Tambah</a>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Kurir</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        
        // ambil data semua kurir
        $q = mysqli_query($conn, "SELECT * FROM kurir ORDER BY id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data kurir
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[nama_kurir]</td>
                    <td>
                        <a href='index.php?halaman=kurir-edit&id=$d[id]'>Edit</a> |
                        <a href='index.php?halaman=kurir-hapus&id=$d[id]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>