<?php
// ambil data semua produk
$q = mysqli_query($conn, "SELECT
    *
    FROM
    produk");

$no = 1;
?>

<div class="text-center">
    <h3>LAPORAN PRODUK TIDAR SPORT</h3>
</div>
<hr>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th width="150px">Gambar</th>
            <th>Nama Produk</th>
            <th>Deskripsi</th>
            <th>Warna</th>
            <th>Harga</th>
            <th>Stok</th>
        </tr>
    </thead>
    <tbody>
        <?php
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data produk
            echo "
                <tr>
                    <td>$no</td>
                    <td><img src='../assets/img/produk/$d[gambar]' class='gambar'></td>
                    <td>$d[nama_produk]</td>
                    <td>$d[deskripsi]</td>
                    <td>$d[warna]</td>
                    <td>".format_rupiah($d["harga"])."</td>
                    <td>$d[stok]</td>
                </tr>
                ";
            $no++;
        }
        ?>
    </tbody>
</table>