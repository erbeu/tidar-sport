<h3>Kota</h3>
<a href="index.php?halaman=kota-tambah" class="btn btn-primary">Tambah</a>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Kota</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        
        // ambil data semua kota
        $q = mysqli_query($conn, "SELECT * FROM kota ORDER BY id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data kota
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[nama_kota]</td>
                    <td>
                        <a href='index.php?halaman=kota-edit&id=$d[id]'>Edit</a> |
                        <a href='index.php?halaman=kota-hapus&id=$d[id]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>