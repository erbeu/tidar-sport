<h3>Admin</h3>
<a href="index.php?halaman=admin-tambah" class="btn btn-primary">Tambah</a>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Admin</th>
            <th>Username</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        
        // ambil data semua admin
        $q = mysqli_query($conn, "SELECT * FROM admin
            ORDER BY id DESC");
        while($d = mysqli_fetch_array($q)) {
            
            // tampilkan data admin
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[nama]</td>
                    <td>$d[username]</td>
                    <td>
                        <a href='index.php?halaman=admin-edit&id=$d[id]'>Edit</a> |
                        <a href='index.php?halaman=admin-hapus&id=$d[id]'>Hapus</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>