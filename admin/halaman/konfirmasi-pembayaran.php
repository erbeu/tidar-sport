<h3>Konfirmasi Pembayaran</h3>
<hr>

<?php
// tampilkan pesan jika ada
echo $msg != null ? "<div class='alert alert-success'>$msg</div>" : "";
?>

<table class="table table-bordered datatable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Pembeli</th>
            
            <th width='100px'>Bukti</th>
            <th>Waktu</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        
        // ambil data semua konfirmasi pembayaran
        $q = mysqli_query($conn, "SELECT
            konfirmasi_pembayaran.id,
            konfirmasi_pembayaran.id_pemesanan,
            pembeli.nama_pembeli,
            konfirmasi_pembayaran.foto,
            konfirmasi_pembayaran.waktu
            FROM
            konfirmasi_pembayaran
            INNER JOIN pemesanan ON pemesanan.id = konfirmasi_pembayaran.id_pemesanan
            INNER JOIN pembeli ON pembeli.id = pemesanan.id_pembeli
            
            ORDER BY pemesanan.id DESC");
        while($d = mysqli_fetch_array($q)) {
            // tampilkan data konfirmasi pembayaran
            echo "
                <tr>
                    <td>$no</td>
                    <td>$d[nama_pembeli]</td>
                    <td><img src='../assets/img/konfirmasi/$d[foto]' width='100px' height='100px' class='img-thumbnail'></td>
                    <td>$d[waktu]</td>
                    <td>
                        <a href='index.php?halaman=konfirmasi-pembayaran-detail&id=$d[id]'>Detail</a>
                    </td>
                </tr>
            ";
            $no++;
        }
        ?>
    </tbody>
</table>