<?php
// ambil data pengembalian barang sesuai id
$q = mysqli_query($conn, "SELECT * FROM pengembalian_barang
    WHERE id = $id");
$d = mysqli_fetch_array($q);
?>

<h3>Detail Pengembalian Barang</h3>
<hr>

<table class="table table-bordered">
    <tr>
        <td width="30%">Waktu Pengembalian</td>
        <td><?php echo $d["waktu"]; ?></td>
    </tr>
    <tr>
        <td>Alasan Pengembalian</td>
        <td><?php echo $d["alasan"]; ?></td>
    </tr>
</table>
<hr>

<?php
$id = $d["id_pemesanan"];
// panggil halaman pemesanan detail
require_once("pemesanan-detail.php");
?>
