<?php
// cek jika tombol simpan ditekan
if(isset($_POST["submit"])) {
    // kirim email ke admin@tidarsport.com
    $headers = "From: admin@tidarsport.com";
    mail($_POST[email_tujuan],$_POST[subjek],$_POST[isi],$headers);
    
    // simpan data pesan keluar
    $q = mysqli_query($conn, "INSERT INTO pesan_keluar VALUES(
        null,
        '$_POST[email_tujuan]',
        '$_POST[subjek]',
        '$_POST[isi]',
        '".date("Y-m-d")."'
    )");
    
    // alihkan ke halaman pesan keluar dan beri pesan berhasil
    header("location:index.php?halaman=pesan-keluar&msg=Pesan Berhasil Dikirim");
}

// ambil data pesan masuk sesuai id
$q = mysqli_query($conn, "SELECT * FROM pesan_masuk WHERE id = $id");
$d = mysqli_fetch_array($q);
?>

<h3>Balas Pesan Masuk</h3>
<hr>
<form action="" method="post">
    <label>Email Tujuan</label>
    <input type="text" name="email_tujuan" class="form-control" value="<?php echo $d["email_masuk"] ?>" required>
    <br>
    <label>Subjek</label>
    <input type="text" name="subjek" class="form-control" value="Re : <?php echo $d["subjek"] ?>" required>
    <br>
    <label>Isi</label>
    <textarea name="isi" class="form-control" required></textarea>
    <br>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
    <a href="index.php?halaman=pesan-masuk" class="btn btn-default">Batal</a>
</form>