<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Print</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/print.css">
</head>

<body>
    <div class="container-fluid">
        <?php
        // panggil file halaman
        require_once("halaman/".$file_halaman.".php");
        ?>

        <button type="button" class="btn btn-success center-block hidden-print" onclick="window.print();"><span class="fa fa-fw fa-print"></span> Cetak Laporan</button>
    </div>

    <!-- js -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>
