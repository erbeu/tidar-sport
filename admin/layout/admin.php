<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Area</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/admin.css">
    <link rel="stylesheet" href="../assets/DataTables/datatables.min.css">
</head>

<body>
    <div class="header">
        <h1><img src="../assets/img/logo.png" alt="" width="50px"> Admin Area</h1>
    </div>

    <div class="container">
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Home</a></li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Master <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?halaman=produk">Produk</a></li>
                            <li><a href="index.php?halaman=kota">Kota</a></li>
                            <li><a href="index.php?halaman=kurir">Kurir</a></li>
                            <li><a href="index.php?halaman=tarif">Tarif</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pemesanan <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?halaman=pemesanan">Pemesanan Produk</a></li>
                            <li><a href="index.php?halaman=pemesanan-custom">Pemesanan Custom</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Konfirmasi Pembayaran <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?halaman=konfirmasi-pembayaran">Produk</a></li>
                            <li><a href="index.php?halaman=konfirmasi-pembayaran-custom">Custom Order</a></li>                        </ul>
                    </li>
                    <li><a href="index.php?halaman=pengembalian-barang">Pengembalian Barang</a></li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pesan <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?halaman=pesan-masuk">Pesan Masuk</a></li>
                            <li><a href="index.php?halaman=pesan-keluar">Pesan Keluar</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Laporan <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?halaman=laporan-penjualan-bulanan">Laporan Penjualan Bulanan</a></li>
                            <li><a href="index.php?halaman=laporan-penjualan-custom-bulanan">Laporan Penjualan Custom Bulanan</a></li>
                            <li><a href="index.php?halaman=laporan-pengembalian-barang-bulanan">Laporan Pengembalian Barang Bulanan</a></li>
                            <li><a href="index.php?halaman=laporan-member&layout=print" target="_blank">Laporan Daftar Member</a></li>
                            <li><a href="index.php?halaman=laporan-produk&layout=print" target="_blank">Laporan Produk</a></li>
                        </ul>
                    </li>
                    <li><a href="index.php?halaman=admin">Admin</a></li>
                    <li><a href="index.php?halaman=member">Member</a></li>
                    <li><a href="index.php?halaman=logout">Logout</a></li>
                </ul>
            </div>
        </nav>
        <div class="inner">
            <?php
            // panggil file halaman
            require_once("halaman/".$file_halaman.".php");
            ?>
        </div>
        <hr>
        <div class="footer text-center">
            <p>Copyright &copy; 2017 TIDAR SPORT</p>
        </div>
    </div>

    <!-- js -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/DataTables/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable();
        });
    </script>
</body>
</html>
