<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tidar Sport</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/DataTables/datatables.min.css">
</head>

<body>
    <div class="container head">
        <div class="header row">
            <h1><img src="assets/img/logo.png" alt="" width="100px"> TIDAR SPORT</h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <nav class="navbar navbar-inverse navbar-static-top">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="index.php?halaman=produk">Produk</a></li>
                        <li><a href="index.php?halaman=custom-order">Custom Order</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Petunjuk Berbelanja <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?halaman=cara-pembelian">Cara Pembelian</a></li>
                                <li><a href="index.php?halaman=cara-pembayaran">Cara Pembayaran</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Konfirmasi Pembayaran <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?halaman=konfirmasi-pembayaran">Produk</a></li>
                                <li><a href="index.php?halaman=konfirmasi-pembayaran-custom">Custom Order</a></li>
                            </ul>
                        </li>
                        <li><a href="index.php?halaman=pengembalian-barang">Pengembalian Barang</a></li>
                        <li><a href="index.php?halaman=hubungi-kami">Hubungi Kami</a></li>
                    </ul>
                </div>
            </nav>
        </div>

        <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="assets/img/banner/0.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="assets/img/banner/1.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="assets/img/banner/2.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="assets/img/banner/3.jpg" alt="...">
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div class="halaman">
            <div class="row">
                <div class="col-md-3 sidebar">
                    <?php
                    // panggil file sidebar
                    require_once("halaman/sidebar.php");
                    ?>
                </div>
                <div class="col-md-9 main-content">
                    <?php
                    // panggil file halaman
                    require_once("halaman/".$file_halaman.".php");
                    ?>
                </div>
            </div>
        </div>

        <div class="footer text-center">
            <p>Copyright &copy; 2017 TIDAR SPORT</p>
        </div>
    </div>

    <!-- js -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.number.min.js"></script>
    <script src="assets/DataTables/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.carousel').carousel();
            $('#dataTables').DataTable();

            <?= isset($js) ? $js : "" ?>
        });
    </script>
</body>
</html>
