-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 16, 2017 at 02:33 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projectonline_tidar-sport`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `username`, `password`) VALUES
(1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(3, 'sukijan', 'sukijan', 'a627fd5294a84ec54c91d3eacab0860b');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasi_pembayaran`
--

CREATE TABLE `konfirmasi_pembayaran` (
  `id` int(11) NOT NULL,
  `id_pemesanan` int(3) UNSIGNED ZEROFILL NOT NULL,
  `foto` varchar(50) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfirmasi_pembayaran`
--

INSERT INTO `konfirmasi_pembayaran` (`id`, `id_pemesanan`, `foto`, `waktu`) VALUES
(1, 001, '001.jpg', '2017-03-20 09:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasi_pembayaran_custom`
--

CREATE TABLE `konfirmasi_pembayaran_custom` (
  `id` int(11) NOT NULL,
  `id_pemesanan_custom` int(3) UNSIGNED ZEROFILL NOT NULL,
  `foto` varchar(50) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfirmasi_pembayaran_custom`
--

INSERT INTO `konfirmasi_pembayaran_custom` (`id`, `id_pemesanan_custom`, `foto`, `waktu`) VALUES
(1, 001, '001.jpg', '2017-10-16 13:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id` int(11) NOT NULL,
  `nama_kota` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id`, `nama_kota`) VALUES
(1, 'Bandung'),
(2, 'Jakarta');

-- --------------------------------------------------------

--
-- Table structure for table `kurir`
--

CREATE TABLE `kurir` (
  `id` int(11) NOT NULL,
  `nama_kurir` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurir`
--

INSERT INTO `kurir` (`id`, `nama_kurir`) VALUES
(1, 'JNE'),
(2, 'Tiki'),
(3, 'J&T');

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `id` int(11) NOT NULL,
  `nama_pembeli` varchar(32) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `telepon` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`id`, `nama_pembeli`, `alamat`, `email`, `telepon`, `username`, `password`) VALUES
(1, 'Joko', '-', 'joko@gmail.com', '0821312873', 'joko', '9ba0009aa81e794e628a04b51eaf7d7f'),
(2, 'Jaka', 'aaaaaaaaa', 'jaka@mailinator.com', '08123123123', 'jakaa', '9d83066da00b7c7fa9de34117f488653');

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `id_pembeli` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `id_kurir` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `biaya_kirim` int(11) NOT NULL,
  `alamat_kirim` text NOT NULL,
  `waktu` datetime NOT NULL,
  `waktu_sampai` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id`, `id_pembeli`, `id_status`, `id_kurir`, `id_kota`, `biaya_kirim`, `alamat_kirim`, `waktu`, `waktu_sampai`) VALUES
(001, 1, 1, 1, 1, 70000, 'p', '2017-06-17 00:22:59', NULL),
(002, 1, 2, 3, 1, 60000, 'a', '2017-06-17 00:29:20', NULL),
(003, 1, 7, 3, 1, 60000, '555', '2017-06-19 12:20:15', NULL),
(004, 1, 7, 3, 1, 90000, 'asdadasd', '2017-08-30 23:16:18', NULL),
(005, 1, 7, 3, 1, 60000, 'aaaaaaaaaaaaaaaa', '2017-08-30 23:58:37', NULL),
(006, 2, 7, 3, 1, 60000, 'cirebon', '2017-09-02 23:10:11', NULL),
(007, 2, 3, 3, 1, 30000, 'rrrrrrrrrr', '2017-09-03 23:21:56', '2017-09-01 23:39:27'),
(008, 2, 7, 3, 1, 30000, '1', '2017-09-02 23:54:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan_custom`
--

CREATE TABLE `pemesanan_custom` (
  `id` int(3) UNSIGNED ZEROFILL NOT NULL,
  `id_pembeli` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `id_kurir` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `biaya_kirim` int(11) NOT NULL,
  `alamat_kirim` text NOT NULL,
  `waktu` datetime NOT NULL,
  `waktu_sampai` datetime DEFAULT NULL,
  `bahan` varchar(10) NOT NULL,
  `harga_bahan` int(11) NOT NULL,
  `ukuran` varchar(1) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `desain_baju` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `nama_no` text NOT NULL,
  `ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan_custom`
--

INSERT INTO `pemesanan_custom` (`id`, `id_pembeli`, `id_status`, `id_kurir`, `id_kota`, `biaya_kirim`, `alamat_kirim`, `waktu`, `waktu_sampai`, `bahan`, `harga_bahan`, `ukuran`, `logo`, `desain_baju`, `jumlah`, `nama_no`, `ket`) VALUES
(001, 1, 3, 3, 1, 30000, 'asdsadasdasdddd', '2017-09-15 00:34:04', NULL, 'Paragon', 100000, 'M', '1df263d996281d984952c07998dc54358-logo.jpg', '1df263d996281d984952c07998dc54358-desain.jpg', 3, 'Joko - 1<br />\r\nJaka - 11', 'asdasdasd');

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan_detail`
--

CREATE TABLE `pemesanan_detail` (
  `id` int(11) NOT NULL,
  `id_pemesanan` int(3) UNSIGNED ZEROFILL NOT NULL,
  `id_produk` int(11) NOT NULL,
  `stok_awal` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `ukuran` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan_detail`
--

INSERT INTO `pemesanan_detail` (`id`, `id_pemesanan`, `id_produk`, `stok_awal`, `jumlah`, `ukuran`) VALUES
(1, 001, 1, 10, 2, 'S'),
(2, 001, 1, 8, 3, 'L'),
(3, 002, 2, 20, 5, 'M'),
(4, 003, 5, 5, 5, 'M'),
(5, 004, 2, 15, 6, 'M'),
(6, 004, 3, 15, 3, 'L'),
(7, 005, 3, 12, 6, 'S'),
(8, 005, 3, 6, 2, 'S'),
(9, 006, 5, 100, 6, 'S'),
(10, 006, 1, 5, 1, 'M'),
(11, 007, 4, 5, 3, 'M'),
(12, 008, 1, 4, 1, 'S');

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian_barang`
--

CREATE TABLE `pengembalian_barang` (
  `id` int(11) NOT NULL,
  `id_pemesanan` int(3) UNSIGNED ZEROFILL NOT NULL,
  `alasan` text NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengembalian_barang`
--

INSERT INTO `pengembalian_barang` (`id`, `id_pemesanan`, `alasan`, `waktu`) VALUES
(1, 007, 'aaaaaaaaaaaaaaaaa', '2017-09-03 00:12:35');

-- --------------------------------------------------------

--
-- Table structure for table `pesan_keluar`
--

CREATE TABLE `pesan_keluar` (
  `id` int(11) NOT NULL,
  `email_tujuan` varchar(50) NOT NULL,
  `subjek` varchar(32) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pesan_masuk`
--

CREATE TABLE `pesan_masuk` (
  `id` int(11) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `email_masuk` varchar(50) NOT NULL,
  `subjek` varchar(32) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama_produk` varchar(32) NOT NULL,
  `deskripsi` text NOT NULL,
  `warna` varchar(10) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `gambar` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `nama_produk`, `deskripsi`, `warna`, `harga`, `stok`, `gambar`) VALUES
(1, 'Jersey Manchester United', '-', 'Merah', 100000, 3, 'Jersey Manchester United.jpg'),
(2, 'Jersey Arsenal', '-', 'Merah', 100000, 9, 'Jersey Arsenal.jpg'),
(3, 'Jersey Barcelona', '-', 'Biru Merah', 100000, 4, 'Jersey Barcelona.jpg'),
(4, 'Jersey Juventus', '-', 'Hitam Puti', 100000, 5, 'Jersey Juventus.jpg'),
(5, 'Jersey Jerman', '-', 'Putih', 120000, 94, 'Jersey Jerman.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `nama_status` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `nama_status`) VALUES
(1, 'Belum Dibayar'),
(2, 'Sedang Dikemas'),
(3, 'Sudah Dikirim'),
(4, 'Sudah Sampai'),
(5, 'Dikembalikan'),
(6, 'Batal'),
(7, 'Selesai'),
(8, 'Sedang Dibuat');

-- --------------------------------------------------------

--
-- Table structure for table `tarif`
--

CREATE TABLE `tarif` (
  `id` int(11) NOT NULL,
  `id_kurir` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `biaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tarif`
--

INSERT INTO `tarif` (`id`, `id_kurir`, `id_kota`, `biaya`) VALUES
(1, 1, 1, 35000),
(2, 1, 2, 35000),
(3, 3, 1, 30000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konfirmasi_pembayaran`
--
ALTER TABLE `konfirmasi_pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pemesanan` (`id_pemesanan`);

--
-- Indexes for table `konfirmasi_pembayaran_custom`
--
ALTER TABLE `konfirmasi_pembayaran_custom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pemesanan_custom` (`id_pemesanan_custom`) USING BTREE;

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kurir`
--
ALTER TABLE `kurir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembeli` (`id_pembeli`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_kurir` (`id_kurir`),
  ADD KEY `id_kota` (`id_kota`);

--
-- Indexes for table `pemesanan_custom`
--
ALTER TABLE `pemesanan_custom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembeli` (`id_pembeli`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_kurir` (`id_kurir`),
  ADD KEY `id_kota` (`id_kota`);

--
-- Indexes for table `pemesanan_detail`
--
ALTER TABLE `pemesanan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pemesanan` (`id_pemesanan`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `pengembalian_barang`
--
ALTER TABLE `pengembalian_barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pemesanan` (`id_pemesanan`);

--
-- Indexes for table `pesan_keluar`
--
ALTER TABLE `pesan_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesan_masuk`
--
ALTER TABLE `pesan_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tarif`
--
ALTER TABLE `tarif`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kurir` (`id_kurir`),
  ADD KEY `id_kota` (`id_kota`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `konfirmasi_pembayaran`
--
ALTER TABLE `konfirmasi_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `konfirmasi_pembayaran_custom`
--
ALTER TABLE `konfirmasi_pembayaran_custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kurir`
--
ALTER TABLE `kurir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pemesanan_custom`
--
ALTER TABLE `pemesanan_custom`
  MODIFY `id` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pemesanan_detail`
--
ALTER TABLE `pemesanan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pengembalian_barang`
--
ALTER TABLE `pengembalian_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pesan_keluar`
--
ALTER TABLE `pesan_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pesan_masuk`
--
ALTER TABLE `pesan_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tarif`
--
ALTER TABLE `tarif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `konfirmasi_pembayaran`
--
ALTER TABLE `konfirmasi_pembayaran`
  ADD CONSTRAINT `konfirmasi_pembayaran_ibfk_1` FOREIGN KEY (`id_pemesanan`) REFERENCES `pemesanan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `konfirmasi_pembayaran_custom`
--
ALTER TABLE `konfirmasi_pembayaran_custom`
  ADD CONSTRAINT `konfirmasi_pembayaran_custom_ibfk_1` FOREIGN KEY (`id_pemesanan_custom`) REFERENCES `pemesanan_custom` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD CONSTRAINT `pemesanan_ibfk_1` FOREIGN KEY (`id_pembeli`) REFERENCES `pembeli` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemesanan_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemesanan_ibfk_3` FOREIGN KEY (`id_kurir`) REFERENCES `kurir` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemesanan_ibfk_4` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pemesanan_detail`
--
ALTER TABLE `pemesanan_detail`
  ADD CONSTRAINT `pemesanan_detail_ibfk_1` FOREIGN KEY (`id_pemesanan`) REFERENCES `pemesanan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemesanan_detail_ibfk_2` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengembalian_barang`
--
ALTER TABLE `pengembalian_barang`
  ADD CONSTRAINT `pengembalian_barang_ibfk_1` FOREIGN KEY (`id_pemesanan`) REFERENCES `pemesanan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tarif`
--
ALTER TABLE `tarif`
  ADD CONSTRAINT `tarif_ibfk_1` FOREIGN KEY (`id_kurir`) REFERENCES `kurir` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tarif_ibfk_2` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
