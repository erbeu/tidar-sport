<?php

// fungsi untuk mengambil nama bulan
function bulan($ke = null) {
    $bulan = array(
        1 => "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember",
    );
    if($ke != null) $bulan = $bulan[$ke];
    return $bulan;
}

// fungsi untuk merubah angka menjadi format rupiah
function format_rupiah($angka){
    $rupiah = number_format($angka,0,',','.');
    return "Rp ".$rupiah.",-";
}

// fungsi untuk menampilkan ukuran produk
function ukuran($i = 0) {
    $ukuran = array("S", "M", "L", "XL");
    if($i != 0) {
        return $ukuran[$i];
    } else {
        return $ukuran;
    }
}
