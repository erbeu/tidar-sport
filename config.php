<?php
//error_reporting(0);

// koneksi ke mysql database
$host = "localhost";
$user = "root";
$pass = "";
$dbnm = "projectonline_tidar-sport";

$conn = mysqli_connect($host, $user, $pass, $dbnm);

// konfigurasi tambahan
$config["min_diskon"] = 6;
$config["persen_diskon"] = 10/100;
$config["batas_pengembalian"] = 7;

$config["bahan_baju"] = array(
    array(
        'nama' => 'Dry Fit',
        'harga' => 120000,
    ),
    array(
        'nama' => 'Paragon',
        'harga' => 100000,
    ),
    array(
        'nama' => 'Serena',
        'harga' => 80000,
    ),
);
?>
